using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelResto.API.Data;
using HotelResto.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace HotelResto.API.Infrastructure.Repository.Utilisateurs
{
    public class UtilisateurRepository : IUtilisateurRepository
    {
        private readonly ApplicationDbContext _context;
        
         public UtilisateurRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<UtilisateurEntity> GetAsync(int id)
        {
            return await _context.UtilisateurEntity.AsNoTracking()
            .Where(Utilisateur => Utilisateur.Id == id)
            .FirstOrDefaultAsync();
        }

        public async Task<UtilisateurEntity> GetByLogin(string login)
        {
            return await _context.UtilisateurEntity.AsNoTracking()
            .Where(user => user.Login == login)
            .FirstOrDefaultAsync();
        }

        public async Task<UtilisateurEntity> AddAsync(UtilisateurEntity utilisateur)
        {
            _context.UtilisateurEntity.Add(utilisateur);
            await _context.SaveChangesAsync();
            return utilisateur;
        }

        public async Task<UtilisateurEntity> Login(UtilisateurEntity user)
        {
            user.Connected = true;
            _context.Update(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public async Task<UtilisateurEntity> Disconnect(UtilisateurEntity user)
        {
            user.Connected = false;
            _context.Update(user);
            await _context.SaveChangesAsync();
            return user;
        }

        public bool Exist(int id)
        {
            return _context.UtilisateurEntity.Any(e => e.Id == id);
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Repository.Utilisateurs
{
    public interface IUtilisateurRepository
    {
#pragma warning disable 1591

        Task<UtilisateurEntity> GetAsync(int id);
        Task<UtilisateurEntity> GetByLogin(string login);
        Task<UtilisateurEntity> AddAsync(UtilisateurEntity example);
        Task<UtilisateurEntity> Login(UtilisateurEntity user);
        Task<UtilisateurEntity> Disconnect(UtilisateurEntity user);
        bool Exist(int id);
    }
}
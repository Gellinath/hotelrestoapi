using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Repository.Restaurant
{
    public interface IRestaurantRepository
    {
         #pragma warning disable 1591

         Task<IEnumerable<RestaurantEntity>> GetAllAsync();
         Task<IEnumerable<RestaurantEntity>> GetTop5Async();
         Task<RestaurantEntity> GetAsync(int id);
         Task<IEnumerable<RestaurantEntity>> GetByNameAsync(string name);
         Task<IEnumerable<RestaurantEntity>> GetByVilleAsync(string ville);
         Task<RestaurantEntity> AddAsync(RestaurantEntity example);
         Task<RestaurantEntity> UpdateAsync(RestaurantEntity example);
         bool Exist(int id);
    }
}
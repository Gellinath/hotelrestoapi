using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelResto.API.Data;
using HotelResto.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace HotelResto.API.Infrastructure.Repository.Restaurant.AvisRestaurant
{
    public class AvisRestaurantRepository : IAvisRestaurantRepository
    {
        private readonly ApplicationDbContext _context;
        
         public AvisRestaurantRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<AvisRestaurantEntity> GetAsync(int id, int idRestaurant)
        {
            return await _context.AvisRestaurantEntity.AsNoTracking()
            .Where(AvisRestaurant => AvisRestaurant.RestaurantId == idRestaurant)
            .Where(AvisRestaurant => AvisRestaurant.Id == id)
            .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<AvisRestaurantEntity>> GetAllAsync(int idRestaurant)
        {
            return await _context.AvisRestaurantEntity
            .Where(AvisRestaurant => AvisRestaurant.RestaurantId == idRestaurant)
            .ToListAsync();
        }

        public async Task<IEnumerable<int>> GetAllNotesAsync(int idRestaurant)
        {
            return await _context.AvisRestaurantEntity
            .Where(AvisRestaurant => AvisRestaurant.RestaurantId == idRestaurant)
            .Select(AvisRestaurant => AvisRestaurant.Note)
            .ToListAsync();
        }

        public async Task<AvisRestaurantEntity> AddAsync(AvisRestaurantEntity avisRestaurant)
        {
            _context.AvisRestaurantEntity.Add(avisRestaurant);
            await _context.SaveChangesAsync();
            return avisRestaurant;
        }

        public bool Exist(int id)
        {
            return _context.AvisRestaurantEntity.Any(e => e.Id == id);
        }
    }
}
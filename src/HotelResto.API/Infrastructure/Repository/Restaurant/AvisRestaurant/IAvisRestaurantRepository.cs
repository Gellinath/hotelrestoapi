using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Repository.Restaurant.AvisRestaurant
{
    public interface IAvisRestaurantRepository
    {
        #pragma warning disable 1591

         Task<IEnumerable<AvisRestaurantEntity>> GetAllAsync(int idRestaurant);
         Task<IEnumerable<int>> GetAllNotesAsync(int idRestaurant);
         Task<AvisRestaurantEntity> GetAsync(int id, int idRestaurant);
         Task<AvisRestaurantEntity> AddAsync(AvisRestaurantEntity example);
         bool Exist(int id);
    }
}
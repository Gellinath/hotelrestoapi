using System.Data.SqlTypes;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Data;
using HotelResto.API.Entities;
using System;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace HotelResto.API.Infrastructure.Repository.Restaurant
{
    public class RestaurantRepository : IRestaurantRepository
    {
        private readonly ApplicationDbContext _context;
        
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="context"></param>
         public RestaurantRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get restaurant by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<RestaurantEntity> GetAsync(int id)
        {
            return await _context.RestaurantEntity.AsNoTracking()
            .Where(Restaurant => Restaurant.Id == id)
            .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Get all restaurant
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetAllAsync()
        {
            return await _context.RestaurantEntity.OrderByDescending(restaurant => restaurant.MoyNote).ToListAsync();
        }

        /// <summary>
        /// Get top 5 of restaurants by notes order descending
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetTop5Async()
        {
            var Top5 = await _context.RestaurantEntity
            .OrderByDescending(restaurant => restaurant.MoyNote).Take(5).ToListAsync();
            return Top5;
            
        }

        /// <summary>
        /// Get restaurants by ville
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetByVilleAsync(string ville)
        {
            var restaurants = await _context.RestaurantEntity
            .OrderByDescending(restaurant => restaurant.MoyNote).Where(restaurants => restaurants.Ville.Contains(ville, StringComparison.InvariantCultureIgnoreCase)).ToListAsync();
            return restaurants;
            
        }

        /// <summary>
        /// Get restaurants by Name
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetByNameAsync(string name)
        {
            var restaurants = await _context.RestaurantEntity
            .OrderByDescending(restaurant => restaurant.MoyNote).Where(restaurants => restaurants.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase)).ToListAsync();
            return restaurants;
            
        }

        /// <summary>
        /// Add restaurant
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public async Task<RestaurantEntity> AddAsync(RestaurantEntity restaurant)
        {
            _context.RestaurantEntity.Add(restaurant);
            await _context.SaveChangesAsync();
            return restaurant;
        }

        /// <summary>
        /// Update restaurant by id
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public async Task<RestaurantEntity> UpdateAsync(RestaurantEntity restaurant)
        {
            _context.RestaurantEntity.Update(restaurant);
            await _context.SaveChangesAsync();
            return restaurant;
        }

        /// <summary>
        /// Verify if restaurant exist by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Exist(int id)
        {
            return _context.RestaurantEntity.Any(e => e.Id == id);
        }
    }
}
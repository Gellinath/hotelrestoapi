using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelResto.API.Data;
using HotelResto.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace HotelResto.API.Infrastructure.Repository.Hotel.AvisHotel
{
    /// <summary>
    /// Avis hotel repository
    /// </summary>
    public class AvisHotelRepository : IAvisHotelRepository
    {
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public AvisHotelRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all avis hotel
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AvisHotelEntity>> GetAllAsync(int idHotel)
        {
            return await _context.AvisHotelEntity
            .Where(hot => hot.Id == idHotel)
            .ToListAsync();
        }

        /// <summary>
        /// Get avis hotel by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<AvisHotelEntity> GetAsync(int id, int idHotel)
        {
            return await _context.AvisHotelEntity.AsNoTracking()
            .Where(hot => hot.Id == idHotel)
            .Where(avishot => avishot.Id == id)
            .FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<int>> GetAllNotesAsync(int idHotel)
        {
            return await _context.AvisHotelEntity
            .Where(AvisHotel => AvisHotel.HotelId == idHotel)
            .Select(AvisHotel => AvisHotel.Note)
            .ToListAsync();
        }

        /// <summary>
        /// Add a new avis hotel
        /// </summary>
        /// <param name="avis"></param>
        /// <returns></returns>
        public async Task<AvisHotelEntity> AddAsync(AvisHotelEntity avis)
        {
            _context.AvisHotelEntity.Add(avis);
            await _context.SaveChangesAsync();
            return avis;
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Repository.Hotel.AvisHotel
{
    public interface IAvisHotelRepository
    {
#pragma warning disable 1591
        Task<IEnumerable<AvisHotelEntity>> GetAllAsync(int idHotel);
        Task<IEnumerable<int>> GetAllNotesAsync(int idHotel);
        Task<AvisHotelEntity> GetAsync(int id, int idHotel);
        Task<AvisHotelEntity> AddAsync(AvisHotelEntity avis);
    }
}
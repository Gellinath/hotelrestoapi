using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelResto.API.Data;
using HotelResto.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace HotelResto.API.Infrastructure.Repository.Hotel.ChambreHotel
{
    public class ChambreHotelRepository : IChambreHotelRepository
    {
        public List<ChambreHotelEntity> test;
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public ChambreHotelRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all chambre of a hotel
        /// </summary>
        /// <param name="idHotel"></param>
        /// <returns>List of all chambre</returns>
        public async Task<IEnumerable<ChambreHotelEntity>> GetAllAsync(int idHotel)
        {
            return await _context.ChambreHotelEntity.Where(ch => ch.HotelId == idHotel)
            .ToListAsync();
        }

        /// <summary>
        /// Get a specific chambre from a hotel
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idHotel"></param>
        /// <returns></returns>
        public async Task<ChambreHotelEntity> GetAsync(int id, int idHotel)
        {
            return await _context.ChambreHotelEntity.AsNoTracking()
            .Where(ch => ch.HotelId == idHotel)
            .Where(ch => ch.Id == id)
            .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Add a chambre to a hotel
        /// </summary>
        /// <param name="chambreHotel"></param>
        /// <param name="idHotel"></param>
        /// <returns></returns>
        public async Task<ChambreHotelEntity> AddAsync(ChambreHotelEntity chambreHotel)
        {
            _context.ChambreHotelEntity.Add(chambreHotel);
            await _context.SaveChangesAsync();
            return chambreHotel;
        }
    }
}
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Repository.Hotel.ChambreHotel
{
    public interface IChambreHotelRepository
    {
#pragma warning disable 1591
        Task<IEnumerable<ChambreHotelEntity>> GetAllAsync(int idHotel);
        Task<ChambreHotelEntity> GetAsync(int id, int idHotel);
        Task<ChambreHotelEntity> AddAsync(ChambreHotelEntity chambreHotel);
    }
}
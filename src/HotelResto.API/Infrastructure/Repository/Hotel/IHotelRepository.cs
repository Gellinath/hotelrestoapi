using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Repository.Hotel
{
    public interface IHotelRepository
    {
#pragma warning disable 1591
        Task<IEnumerable<HotelEntity>> GetAllAsync();
        Task<IEnumerable<HotelEntity>> GetTop5Async();
        Task<HotelEntity> GetAsync(int id);
        Task<IEnumerable<HotelEntity>> GetByNameAsync(string name);
        Task<IEnumerable<HotelEntity>> GetByVilleAsync(string ville);
        Task<HotelEntity> AddAsync(HotelEntity hotel);
        Task<HotelEntity> UpdateAsync(HotelEntity hotel);
        bool Exist(int id);
    }
}
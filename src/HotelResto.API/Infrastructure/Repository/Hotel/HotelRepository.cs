using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelResto.API.Data;
using HotelResto.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace HotelResto.API.Infrastructure.Repository.Hotel
{
    /// <summary>
    /// Hotel repository
    /// </summary>
    public class HotelRepository : IHotelRepository
    {
        private readonly ApplicationDbContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public HotelRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get all hotels
        /// </summary>
        /// <returns>List of all hotels</returns>
        public async Task<IEnumerable<HotelEntity>> GetAllAsync()
        {
            return await _context.HotelEntity.OrderByDescending(hotel => hotel.MoyNote).ToListAsync();
        }

        /// <summary>
        /// Get the hotel with id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Hotel</returns>
        public async Task<HotelEntity> GetAsync(int id)
        {
            return await _context.HotelEntity.AsNoTracking()
                .Where(hot => hot.Id == id)
                .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Get the top 5 best rated hotels
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<HotelEntity>> GetTop5Async()
        {
            var Top5 = await _context.HotelEntity
            .OrderByDescending(hotel => hotel.MoyNote).Take(5).ToListAsync();
            return Top5;
        }

        /// <summary>
        /// Get a hotel by a ville
        /// </summary>
        /// <param name="ville"></param>
        /// <returns></returns>
        public async Task<IEnumerable<HotelEntity>> GetByVilleAsync(string ville)
        {
            var hotels = await _context.HotelEntity
            .OrderByDescending(hotel => hotel.MoyNote).Where(hotels => hotels.Ville.Contains(ville, StringComparison.InvariantCultureIgnoreCase)).ToListAsync();
            return hotels;
        }

        /// <summary>
        /// Get a hotel by a name
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<IEnumerable<HotelEntity>> GetByNameAsync(string name)
        {
            var hotels = await _context.HotelEntity
            .OrderByDescending(hotel => hotel.MoyNote).Where(hotels => hotels.Name.Contains(name, StringComparison.InvariantCultureIgnoreCase)).ToListAsync();
            return hotels;
        }

        /// <summary>
        /// Add a hotel
        /// </summary>
        /// <param name="hotel"></param>
        /// <returns>HotelEntity</returns>
        public async Task<HotelEntity> AddAsync(HotelEntity hotel)
        {
            _context.HotelEntity.Add(hotel);
            await _context.SaveChangesAsync();
            return hotel;
        }

        /// <summary>
        /// Update a hotel
        /// </summary>
        /// <param name="hotel"></param>
        /// <returns></returns>
        public async Task<HotelEntity> UpdateAsync(HotelEntity hotel)
        {
            _context.HotelEntity.Update(hotel);
            await _context.SaveChangesAsync();
            return hotel;
        }

        /// <summary>
        /// Check if hotel exists
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Exist(int id)
        {
            return _context.HotelEntity.Any(e => e.Id == id);
        }
    }
}
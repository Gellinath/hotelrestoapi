using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HotelResto.API.Data;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Exceptions;
using HotelResto.API.Utils.API;
using Microsoft.EntityFrameworkCore;

namespace HotelResto.API.Infrastructure.Repository.ZoneReservation
{
    public class ZoneReservationRepository : IZoneReservationRepository
    {
        private readonly ApplicationDbContext _context;
        
        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="context"></param>
         public ZoneReservationRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Get ZoneReservation by id and user id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ZoneReservationEntity> GetAsync(int id, int userId)
        {
            return await _context.ZoneReservationEntity.AsNoTracking()
            .Where(ZoneReservation => ZoneReservation.Id == id)
            .Where(ZoneReservation => ZoneReservation.UtilisateurId == userId)
            .FirstOrDefaultAsync();
        }

        /// <summary>
        /// Get all ZoneReservation by User Id
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ZoneReservationEntity>> GetAllByUserAsync(int UserId)
        {
            return await _context.ZoneReservationEntity
            .Where(ZoneReservation => ZoneReservation.UtilisateurId == UserId).ToListAsync();
        }

        /// <summary>
        /// Get all ZoneReservation by Hotel Id
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelAsync(int HotelId)
        {
            return await _context.ZoneReservationEntity
            .Where(ZoneReservation => ZoneReservation.HotelId == HotelId).ToListAsync();
        }

        /// <summary>
        /// Get all ZoneReservation by Hotel Id and date
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelAndDateAsync(int HotelId, DateTime arrive, DateTime depart)
        {
            return await _context.ZoneReservationEntity
            .Where(zoneReservation => zoneReservation.HotelId == HotelId)
            .Where(zoneReservation => zoneReservation.Arrive <= depart)
            .Where(zoneReservation => zoneReservation.Depart >= arrive)
            .ToListAsync();
        }

        /// <summary>
        /// Add ZoneReservation by user id
        /// </summary>
        /// <param name="zoneReservation"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ZoneReservationEntity> AddAsync(ZoneReservationEntity zoneReservation, int userId)
        {
            zoneReservation.UtilisateurId = userId;
            _context.ZoneReservationEntity.Add(zoneReservation);
            await _context.SaveChangesAsync();
            return zoneReservation;
        }

        /// <summary>
        /// Delete ZoneReservation by user id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<ZoneReservationEntity> DeleteAsync(ZoneReservationEntity zoneReservation)
        {

            _context.ZoneReservationEntity.Remove(zoneReservation);
            await _context.SaveChangesAsync();
            return zoneReservation;
        }

        /// <summary>
        /// Verify if zoneReservation exist by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public bool Exist(int id)
        {
            return _context.RestaurantEntity.Any(e => e.Id == id);
        }
    }
}
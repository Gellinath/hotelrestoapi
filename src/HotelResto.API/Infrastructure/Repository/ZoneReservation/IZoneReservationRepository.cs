using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Repository.ZoneReservation
{
    public interface IZoneReservationRepository
    {
         #pragma warning disable 1591
        Task<ZoneReservationEntity> GetAsync(int id, int userId);
        Task<IEnumerable<ZoneReservationEntity>> GetAllByUserAsync(int UserId);
        Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelAsync(int HotelId);
        Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelAndDateAsync(int HotelId, DateTime arrive, DateTime depart);
        Task<ZoneReservationEntity> AddAsync(ZoneReservationEntity zoneReservation, int userId);
        Task<ZoneReservationEntity> DeleteAsync(ZoneReservationEntity zoneReservation);
        
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Services.ZoneReservation
{
    public interface IZoneReservationService
    {
         #pragma warning disable 1591
         Task<ZoneReservationEntity> GetZoneReservationByIdAndUserId(int id, int userId);
         Task<IEnumerable<ZoneReservationEntity>> GetAllByUserIdZoneReservation(int userId);
         Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelIdZoneReservation(int hotelId);
         Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelIdAndDateZoneReservation(int hotelId, DateTime arrive, DateTime depart);
         Task<ZoneReservationEntity> AddZoneReservation(ZoneReservationEntity zoneReservation, int userId);
         Task<ZoneReservationEntity> DeleteZoneReservation(int id, int userId);
         
    }
}
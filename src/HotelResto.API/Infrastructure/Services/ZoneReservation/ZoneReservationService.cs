using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Exceptions;
using HotelResto.API.Infrastructure.Repository.ZoneReservation;
using HotelResto.API.Utils.API;

namespace HotelResto.API.Infrastructure.Services.ZoneReservation
{
    public class ZoneReservationService : IZoneReservationService
    {
        private readonly IZoneReservationRepository _zoneReservationRepository;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="zoneReservationRepository"></param>
        public ZoneReservationService(IZoneReservationRepository zoneReservationRepository)
        {
            _zoneReservationRepository = zoneReservationRepository ??
            throw new ArgumentNullException(nameof(zoneReservationRepository));

        }

        /// <summary>
        /// Get ZoneReservation by id and user id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ZoneReservationEntity> GetZoneReservationByIdAndUserId(int id, int userId)
        {
            return await _zoneReservationRepository.GetAsync(id, userId);
        }

        /// <summary>
        /// Get all ZoneReservation by User Id
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ZoneReservationEntity>> GetAllByUserIdZoneReservation(int userId)
        {
            return await _zoneReservationRepository.GetAllByUserAsync(userId);
        }

        /// <summary>
        /// Get all ZoneReservation by Hotel Id
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelIdZoneReservation(int hotelId)
        {
            return await _zoneReservationRepository.GetAllByHotelAsync(hotelId);
        }

        /// <summary>
        /// Get all ZoneReservation by Hotel Id and date
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<ZoneReservationEntity>> GetAllByHotelIdAndDateZoneReservation(int hotelId, DateTime arrive, DateTime depart)
        {
            return await _zoneReservationRepository.GetAllByHotelAndDateAsync(hotelId, arrive, depart);
        }

        /// <summary>
        /// Add ZoneReservation by user id
        /// </summary>
        /// <param name="zoneReservation"></param>
        /// <returns></returns>
        public async Task<ZoneReservationEntity> AddZoneReservation(ZoneReservationEntity zoneReservation, int userId)
        {
            return await _zoneReservationRepository.AddAsync(zoneReservation, userId);
        }

        /// <summary>
        /// Delete ZoneReservation by user id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<ZoneReservationEntity> DeleteZoneReservation(int id, int userId)
        {
            var zoneReservation = await GetZoneReservationByIdAndUserId(id, userId);

            if (zoneReservation is null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }

            return await _zoneReservationRepository.DeleteAsync(zoneReservation);
        }
    }
}
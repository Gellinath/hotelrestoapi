using System.Diagnostics;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Services.Utilisateurs
{
    public interface IUtilisateurService
    {
         #pragma warning disable 1591

         Task<UtilisateurEntity> GetUtilisateur(int id);
         Task<UtilisateurEntity> GetUtilisateurByLogin(string login);
         Task<UtilisateurEntity> AddUtilisateur(UtilisateurEntity example);
         Task<UtilisateurEntity> Login(UtilisateurEntity user);
         Task<UtilisateurEntity> Disconnect(UtilisateurEntity user);
    }
}
using System;
using System.Threading.Tasks;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Repository.Utilisateurs;

namespace HotelResto.API.Infrastructure.Services.Utilisateurs
{
    public class UtilisateurService : IUtilisateurService
    {
        private readonly IUtilisateurRepository _utilisateurRepository;

        public UtilisateurService(IUtilisateurRepository utilisateurRepository)
        {
            _utilisateurRepository = utilisateurRepository ??
            throw new ArgumentNullException(nameof(utilisateurRepository));

        }

        public async Task<UtilisateurEntity> GetUtilisateur(int id)
        {
            return await _utilisateurRepository.GetAsync(id);
        }

        public async Task<UtilisateurEntity> GetUtilisateurByLogin(string login)
        {
            return await _utilisateurRepository.GetByLogin(login);
        }

        public async Task<UtilisateurEntity> AddUtilisateur(UtilisateurEntity utilisateur)
        {
            return await _utilisateurRepository.AddAsync(utilisateur);
        }

        public async Task<UtilisateurEntity> Login(UtilisateurEntity user)
        {
            return await _utilisateurRepository.Login(user);
        }

        public async Task<UtilisateurEntity> Disconnect(UtilisateurEntity user)
        {
            return await _utilisateurRepository.Disconnect(user);
        }
    }
}
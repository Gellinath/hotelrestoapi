using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Services.Hotel.Avis
{
    public interface IAvisHotelService
    {
#pragma warning disable 1591
        Task<IEnumerable<AvisHotelEntity>> GetAllAvisHotel(int idHotel);
        Task<IEnumerable<int>> GetAllNotesAvisHotel(int idHotel);
        Task<AvisHotelEntity> GetAvisHotelById(int id, int idHotel);
        Task<AvisHotelEntity> AddAvisHotel(AvisHotelEntity avisHotel, int idHotel);
    }
}
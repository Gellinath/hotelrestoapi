using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Exceptions;
using HotelResto.API.Infrastructure.Repository.Hotel;
using HotelResto.API.Infrastructure.Repository.Hotel.AvisHotel;
using HotelResto.API.Utils.API;

namespace HotelResto.API.Infrastructure.Services.Hotel.Avis
{
    /// <summary>
    /// Avis hotel service
    /// </summary>
    public class AvisHotelService : IAvisHotelService
    {
        private readonly IAvisHotelRepository _avisHotelRepository;

        private readonly IHotelRepository _hotelRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="avisHotelRepository"></param>
        public AvisHotelService(IAvisHotelRepository avisHotelRepository, IHotelRepository hotelRepository)
        {
            _avisHotelRepository = avisHotelRepository ?? throw new ArgumentNullException(nameof(avisHotelRepository));
            _hotelRepository = hotelRepository ?? throw new ArgumentNullException(nameof(hotelRepository));
        }

        /// <summary>
        /// Get all avis hotel
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<AvisHotelEntity>> GetAllAvisHotel(int idHotel)
        {
            if (await _hotelRepository.GetAsync(idHotel) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else
            {
                return await _avisHotelRepository.GetAllAsync(idHotel);
            }
        }

        /// <summary>
        /// Get avis hotel by unique identifier
        /// </summary>
        /// <param name="id">Avis hotel unique identifier</param>
        /// <returns></returns>
        public async Task<AvisHotelEntity> GetAvisHotelById(int id, int idHotel)
        {
            if (await _hotelRepository.GetAsync(idHotel) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else
            {
                return await _avisHotelRepository.GetAsync(id, idHotel);
            }
        }

        public async Task<IEnumerable<int>> GetAllNotesAvisHotel(int idHotel)
        {
            if (await _hotelRepository.GetAsync(idHotel) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else {
            
            return await _avisHotelRepository.GetAllNotesAsync(idHotel);
            }
        }

        /// <summary>
        /// Add avis hotel
        /// </summary>
        /// <param name="avisHotel"></param>
        /// <returns></returns>
        public async Task<AvisHotelEntity> AddAvisHotel(AvisHotelEntity avisHotel, int idHotel)
        {
            if (await _hotelRepository.GetAsync(idHotel) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else
            {
                return await _avisHotelRepository.AddAsync(avisHotel);
            }
        }
    }
}
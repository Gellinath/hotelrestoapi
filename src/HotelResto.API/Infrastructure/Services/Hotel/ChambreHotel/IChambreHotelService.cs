using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Services.Hotel.ChambreHotel
{
    public interface IChambreHotelService
    {
#pragma warning disable 1591
        Task<IEnumerable<ChambreHotelEntity>> GetAllChambreHotel(int idHotel);
        Task<ChambreHotelEntity> GetChambreHotelById(int id, int idHotel);
        Task<ChambreHotelEntity> AddChambreHotel(ChambreHotelEntity avisHotel, int idHotel);
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Exceptions;
using HotelResto.API.Infrastructure.Repository.Hotel;
using HotelResto.API.Infrastructure.Repository.Hotel.ChambreHotel;
using HotelResto.API.Utils.API;

namespace HotelResto.API.Infrastructure.Services.Hotel.ChambreHotel
{
    /// <summary>
    /// Chambre hotel service
    /// </summary>
    public class ChambreHotelService : IChambreHotelService
    {
        private readonly IChambreHotelRepository _chambreHotelRepository;

        private readonly IHotelRepository _hotelRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="chambreHotelRepository"></param>
        /// <param name="hotelRepository"></param>
        public ChambreHotelService(IChambreHotelRepository chambreHotelRepository, IHotelRepository hotelRepository)
        {
            _chambreHotelRepository = chambreHotelRepository ?? throw new ArgumentNullException(nameof(chambreHotelRepository));
            _hotelRepository = hotelRepository ?? throw new ArgumentException(nameof(hotelRepository));
        }

        /// <summary>
        /// Get all chambre by hotel id
        /// </summary>
        /// <param name="idHotel"></param>
        /// <returns>List of all chambre</returns>
        public async Task<IEnumerable<ChambreHotelEntity>> GetAllChambreHotel(int idHotel)
        {
            if (await _hotelRepository.GetAsync(idHotel) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else 
            {
                return await _chambreHotelRepository.GetAllAsync(idHotel);
            }
        }

        /// <summary>
        /// Get chambre by id and hotel id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="idHotel"></param>
        /// <returns>Chambre</returns>
        public async Task<ChambreHotelEntity> GetChambreHotelById(int id, int idHotel)
        {
            if (await _hotelRepository.GetAsync(idHotel) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else 
            {
                return await _chambreHotelRepository.GetAsync(id, idHotel);
            }
        }

        /// <summary>
        /// Add a chambre to a hotel
        /// </summary>
        /// <param name="chambreHotel"></param>
        /// <param name="idHotel"></param>
        /// <returns>Created chambre</returns>
        public async Task<ChambreHotelEntity> AddChambreHotel(ChambreHotelEntity chambreHotel, int idHotel)
        {
            if (await _hotelRepository.GetAsync(idHotel) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else 
            {
                return await _chambreHotelRepository.AddAsync(chambreHotel);
            }
        }
    }
}
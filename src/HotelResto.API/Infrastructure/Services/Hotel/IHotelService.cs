using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Services.Hotel
{
    public interface IHotelService
    {
#pragma warning disable 1591
        Task<IEnumerable<HotelEntity>> GetAllHotels();
        Task<IEnumerable<HotelEntity>> GetTop5Hotel();
        Task<HotelEntity> GetHotelById(int id);
        Task<IEnumerable<HotelEntity>> GetHotelByName(string name);
         Task<IEnumerable<HotelEntity>> GetHotelByVille(string ville);
        Task<HotelEntity> AddHotel(HotelEntity hotel);
        Task<HotelEntity> PutHotel(HotelEntity hotel);
    }
}
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Repository.Hotel;

namespace HotelResto.API.Infrastructure.Services.Hotel
{
    /// <summary>
    /// Hotel service
    /// </summary>
    public class HotelService : IHotelService
    {
        private readonly IHotelRepository _hotelRepository;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotel"></param>
        public HotelService(IHotelRepository hotelRepository)
        {
            _hotelRepository = hotelRepository ??
            throw new ArgumentNullException(nameof(hotelRepository));
        }

        /// <summary>
        /// Get all hotels
        /// </summary>
        /// <returns>List of all hotels</returns>
        public async Task<IEnumerable<HotelEntity>> GetAllHotels()
        {
            return await _hotelRepository.GetAllAsync();
        }

        /// <summary>
        /// Get hotel by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>HotelEntity</returns>
        public async Task<HotelEntity> GetHotelById(int id)
        {
            return await _hotelRepository.GetAsync(id);
        }

        /// <summary>
        /// Get top 5 best rated hotels
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<HotelEntity>> GetTop5Hotel()
        {
            return await _hotelRepository.GetTop5Async();
        }

        /// <summary>
        /// Get hotel by ville
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<HotelEntity>> GetHotelByVille(string ville)
        {
            return await _hotelRepository.GetByVilleAsync(ville);
        }

        /// <summary>
        /// Get hotel by name
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<HotelEntity>> GetHotelByName(string name)
        {
            return await _hotelRepository.GetByNameAsync(name);
        }

        /// <summary>
        /// Add Hotel
        /// </summary>
        /// <param name="hotel"></param>
        /// <returns>New hotel</returns>
        public async Task<HotelEntity> AddHotel(HotelEntity hotel)
        {
            return await _hotelRepository.AddAsync(hotel);
        }

        /// <summary>
        /// Update hotel
        /// </summary>
        /// <param name="hotel"></param>
        /// <returns></returns>
        public async Task<HotelEntity> PutHotel(HotelEntity hotel)
        {
            return await _hotelRepository.UpdateAsync(hotel);
        }
    }
}
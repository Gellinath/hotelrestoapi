using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Services.Restaurant
{
    public interface IRestaurantService
    {
         #pragma warning disable 1591

         Task<IEnumerable<RestaurantEntity>> GetAllRestaurant();
         Task<IEnumerable<RestaurantEntity>> GetTop5Restaurant();
         Task<RestaurantEntity> GetRestaurant(int id);
         Task<IEnumerable<RestaurantEntity>> GetRestaurantByName(string name);
         Task<IEnumerable<RestaurantEntity>> GetRestaurantByVille(string ville);
         Task<RestaurantEntity> AddRestaurant(RestaurantEntity example);
        Task<RestaurantEntity> PutRestaurant(RestaurantEntity example);
    }
}
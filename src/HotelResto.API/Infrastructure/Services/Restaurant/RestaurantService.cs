using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Repository.Restaurant;

namespace HotelResto.API.Infrastructure.Services.Restaurant
{
    public class RestaurantService : IRestaurantService
    {
        private readonly IRestaurantRepository _restaurantRepository;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="restaurantRepository"></param>
        public RestaurantService(IRestaurantRepository restaurantRepository)
        {
            _restaurantRepository = restaurantRepository ??
            throw new ArgumentNullException(nameof(restaurantRepository));

        }

        /// <summary>
        /// Get restaurant by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<RestaurantEntity> GetRestaurant(int id)
        {
            return await _restaurantRepository.GetAsync(id);
        }

        /// <summary>
        /// Get all restaurants
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetAllRestaurant()
        {
            return await _restaurantRepository.GetAllAsync();
        }

        /// <summary>
        /// Get top 5 of restaurants by note
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetTop5Restaurant()
        {
            return await _restaurantRepository.GetTop5Async();
        }

        /// <summary>
        /// Get restaurants by ville
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetRestaurantByVille(string ville)
        {
            return await _restaurantRepository.GetByVilleAsync(ville);
        }

        /// <summary>
        /// Get restaurants by name
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<RestaurantEntity>> GetRestaurantByName(string name)
        {
            return await _restaurantRepository.GetByNameAsync(name);
        }

        /// <summary>
        /// Add restaurant
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public async Task<RestaurantEntity> AddRestaurant(RestaurantEntity restaurant)
        {
            return await _restaurantRepository.AddAsync(restaurant);
        }

        /// <summary>
        /// Update restaurant
        /// </summary>
        /// <param name="restaurant"></param>
        /// <returns></returns>
        public async Task<RestaurantEntity> PutRestaurant(RestaurantEntity restaurant)
        {
            return await _restaurantRepository.UpdateAsync(restaurant);
        }
    }
}
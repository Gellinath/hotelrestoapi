using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Exceptions;
using HotelResto.API.Infrastructure.Repository.Restaurant;
using HotelResto.API.Infrastructure.Repository.Restaurant.AvisRestaurant;
using HotelResto.API.Utils.API;

namespace HotelResto.API.Infrastructure.Services.Restaurant.AvisRestaurant
{
    public class AvisRestaurantService : IAvisRestaurantService
    {
        private readonly IAvisRestaurantRepository _avisRestaurantRepository;
        private readonly IRestaurantRepository _RestaurantRepository;

        public AvisRestaurantService(IAvisRestaurantRepository avisRestaurantRepository, IRestaurantRepository restaurantRepository)
        {
            _avisRestaurantRepository = avisRestaurantRepository ??
            throw new ArgumentNullException(nameof(avisRestaurantRepository));
            
             _RestaurantRepository = restaurantRepository ??
            throw new ArgumentNullException(nameof(RestaurantRepository));
        }

        public async Task<AvisRestaurantEntity> GetAvisRestaurant(int id, int idRestaurant)
        {
            if (await _RestaurantRepository.GetAsync(idRestaurant) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else{
            return await _avisRestaurantRepository.GetAsync(id, idRestaurant);
            }
        }

        public async Task<IEnumerable<AvisRestaurantEntity>> GetAllAvisRestaurant(int idRestaurant)
        {
            if (await _RestaurantRepository.GetAsync(idRestaurant) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else{
            return await _avisRestaurantRepository.GetAllAsync(idRestaurant);
            }
        }

        public async Task<IEnumerable<int>> GetAllNotesAvisRestaurant(int idRestaurant)
        {
            if (await _RestaurantRepository.GetAsync(idRestaurant) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else {
            
            return await _avisRestaurantRepository.GetAllNotesAsync(idRestaurant);
            }
        }

        public async Task<AvisRestaurantEntity> AddAvisRestaurant(AvisRestaurantEntity avisRestaurant, int idRestaurant)
        {
            if (await _RestaurantRepository.GetAsync(idRestaurant) == null)
            {
                throw new NotFoundException(ExceptionMessageUtils.NOT_FOUND);
            }
            else{
            return await _avisRestaurantRepository.AddAsync(avisRestaurant);
            }
        }
    }
}
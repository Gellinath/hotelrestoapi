using System.Collections.Generic;
using System.Threading.Tasks;
using HotelResto.API.Entities;

namespace HotelResto.API.Infrastructure.Services.Restaurant.AvisRestaurant
{
    public interface IAvisRestaurantService
    {
         #pragma warning disable 1591

         Task<IEnumerable<AvisRestaurantEntity>> GetAllAvisRestaurant(int idRestaurant);
         Task<IEnumerable<int>> GetAllNotesAvisRestaurant(int idRestaurant);
         Task<AvisRestaurantEntity> GetAvisRestaurant(int id, int idRestaurant);
         Task<AvisRestaurantEntity> AddAvisRestaurant(AvisRestaurantEntity example, int idRestaurant);
    }
}
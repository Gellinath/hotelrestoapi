using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel.AvisHotel
{
    /// <summary>
    /// DTO avis hotel for specific get
    /// </summary>
    public class AvisHotelDTO : AvisHotelBase
    {
        /// <summary>
        /// Avis hotel id
        /// </summary>
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
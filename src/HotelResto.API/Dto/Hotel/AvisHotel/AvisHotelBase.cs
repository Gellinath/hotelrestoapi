using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel.AvisHotel
{
    /// <summary>
    /// Base avis hotel DTO
    /// </summary>
    public abstract class AvisHotelBase
    {
        /// <summary>
        /// Avis hotel description
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }
        
        /// <summary>
        /// Avis hotel note
        /// </summary>
        [JsonProperty("note")]
        public int Note { get; set; }

        /// <summary>
        /// Avis hotel hotelId
        /// </summary>
        [JsonProperty("hotelId")]
        public int HotelId { get; set; }
    }
}
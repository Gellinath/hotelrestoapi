using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel
{
    public class HotelTop5DTO : HotelDTO
    {
        [JsonProperty("nbAvis")]
        public int nbAvis {get; set;}
    }
}
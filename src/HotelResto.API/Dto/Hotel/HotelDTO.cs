using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel
{
    /// <summary>
    /// Hotel DTO
    /// </summary>
    public class HotelDTO : HotelBase
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("moyenneNote")]
        public int MoyNote {get; set;}

        /// <summary>
        /// All rooms in the hotel
        /// </summary>
        [JsonProperty("roomsAvailable")]
        public int RoomsAvailable { get; set; }
    }
}
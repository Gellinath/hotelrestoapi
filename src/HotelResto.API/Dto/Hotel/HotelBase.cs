using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel
{
    /// <summary>
    /// Hotel base DTO
    /// </summary>
    public abstract class HotelBase
    {
        /// <summary>
        /// Hotel name
        /// </summary>
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Hotel description
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Hotel num rue
        /// </summary>
        [JsonProperty("numRue")]
        public string NumRue { get; set; }

        /// <summary>
        /// Hotel name rue
        /// </summary>
        [JsonProperty("nameRue")]
        public string NameRue { get; set; }

        /// <summary>
        /// Hotel code postal
        /// </summary>
        [JsonProperty("cp")]
        public string CP { get; set; }

        /// <summary>
        /// Hotel ville
        /// </summary>
        [JsonProperty("ville")]
        public string Ville { get; set; }

        /// <summary>
        /// Hotel email
        /// </summary>
        [JsonProperty("email")]
        public string Email { get; set; }

        /// <summary>
        /// Hotel phone number
        /// </summary>
        [JsonProperty("number")]
        public string Number { get; set; }

        /// <summary>
        /// Hotel image
        /// </summary>
        [JsonProperty("image")]
        public string Image { get; set; }

    }
}
using System.Collections.Generic;
using HotelResto.API.Entities;
using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel
{
    public class HotelAndAvisDTO : HotelDTO
    {
        [JsonProperty("avisHotel")]
        public IEnumerable<AvisHotelEntity> AvisHotel { get; set; }
    }
}
using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel.ChambreHotel
{
    /// <summary>
    /// Chambre hotel base DTO
    /// </summary>
    public class ChambreHotelBase
    {
        /// <summary>
        /// Chambre hotel description
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Chambre hotel description
        /// </summary>
        [JsonProperty("size")]
        public int Size { get; set; }

        /// <summary>
        /// Chambre hotel id
        /// </summary>
        [JsonProperty("hotelId")]
        public int HotelId { get; set; }
    }
}
using Newtonsoft.Json;

namespace HotelResto.API.Dto.Hotel.ChambreHotel
{
    /// <summary>
    /// Chambre hotel DTO
    /// </summary>
    public class ChambreHotelDTO : ChambreHotelBase
    {
        [JsonProperty("id")]
        public int Id { get; set; }
    }
}
using System.Collections.Generic;
using HotelResto.API.Entities;
using Newtonsoft.Json;

namespace HotelResto.API.Dto.Restaurant
{
    public class RestaurantAndAvisDTO : RestaurantDTO
    {
        [JsonProperty("avis")]
        public IEnumerable<AvisRestaurantEntity> Avis {get; set;}
    }
}
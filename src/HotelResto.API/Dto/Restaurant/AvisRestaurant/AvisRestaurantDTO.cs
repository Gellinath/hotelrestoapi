using Newtonsoft.Json;

namespace HotelResto.API.Dto.Restaurant.AvisRestaurant
{
    public class AvisRestaurantDTO : AvisRestaurantBase
    {
        [JsonProperty("id")]
        public string Id {get; set;}
    }
}
using Newtonsoft.Json;

namespace HotelResto.API.Dto.Restaurant.AvisRestaurant
{
    public abstract class AvisRestaurantBase
    {
        [JsonProperty("description")]
        public string Description {get; set;}

        [JsonProperty("note")]
        public string Note {get; set;}

        [JsonProperty("restaurant_id")]
        public string RestaurantId {get; set;}
    }
}
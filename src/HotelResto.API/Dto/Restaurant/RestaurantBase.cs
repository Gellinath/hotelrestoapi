using HotelResto.API.Entities;
using Newtonsoft.Json;

namespace HotelResto.API.Dto.Restaurant
{
    public abstract class RestaurantBase
    {
        
        [JsonProperty("name")]
        public string Name {get; set;}

        [JsonProperty("description")]
        public string Description {get; set;}

        [JsonProperty("image")]
        public string Image {get; set;}

        [JsonProperty("telephone")]
        public string Telephone {get; set;}

        [JsonProperty("email")]
        public string Email {get; set;}

        [JsonProperty("num_rue")]
        public string NumRue {get; set;}

        [JsonProperty("name_rue")]
        public string NameRue {get; set;}

        [JsonProperty("cp")]
        public string CP {get; set;}

        [JsonProperty("ville")]
        public string Ville {get; set;}

    }
}
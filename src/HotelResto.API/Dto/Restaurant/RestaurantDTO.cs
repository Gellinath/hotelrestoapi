using System.Collections.Generic;
using HotelResto.API.Dto.Restaurant;
using HotelResto.API.Dto.Restaurant.AvisRestaurant;
using HotelResto.API.Entities;
using Newtonsoft.Json;

namespace HotelResto.API.Dto.Restaurant
{
    public class RestaurantDTO : RestaurantBase
    {
        [JsonProperty("id")]
        public int Id {get; set;}

        [JsonProperty("moyenneNote")]
        public double MoyNote {get; set;}

    }
}
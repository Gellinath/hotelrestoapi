using Newtonsoft.Json;

namespace HotelResto.API.Dto.Restaurant
{
    public class RestaurantTop5DTO : RestaurantDTO
    {
        [JsonProperty("nbAvis")]
        public int nbAvis {get; set;}
    }
}
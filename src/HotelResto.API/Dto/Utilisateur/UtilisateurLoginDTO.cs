using Newtonsoft.Json;

namespace HotelResto.API.Dto.Utilisateur
{
    /// <summary>
    /// User login DTO
    /// </summary>
    public class UtilisateurLoginDTO
    {
        /// <summary>
        /// User Login
        /// </summary>
        [JsonProperty("login")]
        public string Login {get; set;}

        /// <summary>
        /// User password
        /// </summary>
        [JsonProperty("mdp")]
        public string MDP {get; set;}
    }
}
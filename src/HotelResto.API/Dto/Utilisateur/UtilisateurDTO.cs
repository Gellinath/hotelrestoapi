using Newtonsoft.Json;

namespace HotelResto.API.Dto.Utilisateur
{
    public class UtilisateurDTO : UtilisateurBase
    {
        [JsonProperty("id")]
        public string Id {get; set;}
    }
}
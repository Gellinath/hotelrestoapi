using Newtonsoft.Json;

namespace HotelResto.API.Dto.Utilisateur
{
    public class UtilisateurBase
    {
        [JsonProperty("nom")]
        public string Nom {get; set;}

        [JsonProperty("prenom")]
        public string Prenom {get; set;}

        [JsonProperty("telephone")]
        public string Telephone {get; set;}

        [JsonProperty("email")]
        public string Email {get; set;}

        [JsonProperty("login")]
        public string Login {get; set;}

        [JsonProperty("mdp")]
        public string MDP {get; set;}

        [JsonProperty("connected")]
        public bool Connected { get; set; }        
    }
}
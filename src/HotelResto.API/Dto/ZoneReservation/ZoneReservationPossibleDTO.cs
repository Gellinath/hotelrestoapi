using System;
using Newtonsoft.Json;

namespace HotelResto.API.Dto.ZoneReservation
{
    public class ZoneReservationPossibleDTO
    {

        [JsonProperty("arrive")]
        public DateTime Arrive {get; set;}

        [JsonProperty("depart")]
        public DateTime Depart {get; set;}

        [JsonProperty("nbrChambre")]
        public int NbrChambre {get; set;}

        [JsonProperty("nbrPersonne")]
        public int NbrPersonne {get; set;}
    }
}
using Newtonsoft.Json;

namespace HotelResto.API.Dto.ZoneReservation
{
    public class ZoneReservationDTO : ZoneReservationBase
    {
        [JsonProperty("id")]
        public int id {get; set;}

        [JsonProperty("utilisateurId")]
        public int UtilisateurId {get; set;}
    }
}
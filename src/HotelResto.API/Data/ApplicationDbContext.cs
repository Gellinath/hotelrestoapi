using HotelResto.API.Entities;
using Microsoft.EntityFrameworkCore;

namespace HotelResto.API.Data
{
    /// <summary>
    /// Application db context
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
    
        /// <summary>
        /// constructor
        /// </summary>
        /// <param name="options"></param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<AvisHotelEntity> AvisHotelEntity {get; set;}
        public DbSet<AvisRestaurantEntity> AvisRestaurantEntity {get; set;}
        public DbSet<ChambreHotelEntity> ChambreHotelEntity {get; set;}
        public DbSet<HotelEntity> HotelEntity {get; set;}
        public DbSet<RestaurantEntity> RestaurantEntity {get; set;}
        public DbSet<UtilisateurEntity> UtilisateurEntity {get; set;}
        public DbSet<ZoneReservationEntity> ZoneReservationEntity {get; set;}

        /// <summary>
        /// On Model Creating
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            //Many to one between Chambre hotel and Hotel
            builder.Entity<ChambreHotelEntity>()
            .HasOne(ch => ch.Hotel)
            .WithMany(h => h.ChambreHotel)
            .HasForeignKey(ch => ch.HotelId);

            //Many to one between Avis hotel and Hotel
            builder.Entity<AvisHotelEntity>()
            .HasOne(avis => avis.Hotel)
            .WithMany(h => h.AvisHotel)
            .HasForeignKey(avis => avis.HotelId);

            builder.Entity<UtilisateurEntity>()
            .HasIndex(user => user.Login)
            .IsUnique();
        }
    }
}
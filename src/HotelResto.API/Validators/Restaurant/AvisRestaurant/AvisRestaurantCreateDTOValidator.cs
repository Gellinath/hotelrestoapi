using FluentValidation;
using HotelResto.API.Dto.Restaurant.AvisRestaurant;

namespace HotelResto.API.Validators.Restaurant.Avis
{
    public class AvisRestaurantCreateDTOValidator : AbstractValidator<AvisRestaurantCreateDTO>
    {
        public AvisRestaurantCreateDTOValidator()
        {
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Note).NotEmpty();
            RuleFor(x => x.RestaurantId).NotEmpty();
        }
    }
}
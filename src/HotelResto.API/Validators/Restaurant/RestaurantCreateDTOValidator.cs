using FluentValidation;
using HotelResto.API.Dto.Restaurant;

namespace HotelResto.API.Validators.Restaurant
{
    public class RestaurantCreateDTOValidator : AbstractValidator<RestaurantCreateDTO>
    {
        public RestaurantCreateDTOValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Image);
            RuleFor(x => x.Telephone);
            RuleFor(x => x.Email);
            RuleFor(x => x.NumRue).NotEmpty();
            RuleFor(x => x.NameRue).NotEmpty();
            RuleFor(x => x.CP).NotEmpty();
            RuleFor(x => x.Ville).NotEmpty();
        }
    }
}
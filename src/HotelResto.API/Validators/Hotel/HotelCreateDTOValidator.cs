using FluentValidation;
using HotelResto.API.Dto.Hotel;

namespace HotelResto.API.Validators.Hotel
{
    /// <summary>
    /// Create hotel DTO validator
    /// </summary>
    public class HotelCreateDTOValidator : AbstractValidator<HotelCreateDTO>
    {
        public HotelCreateDTOValidator()
        {
            RuleFor(x => x.Name).NotEmpty();
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.NumRue).NotEmpty();
            RuleFor(x => x.NameRue).NotEmpty();
            RuleFor(x => x.CP).NotEmpty();
            RuleFor(x => x.Ville).NotEmpty();
        }
    }
}
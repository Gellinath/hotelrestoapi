using FluentValidation;
using HotelResto.API.Dto.Hotel.AvisHotel;

namespace HotelResto.API.Validators.Hotel.Avis
{
    /// <summary>
    /// Validator for avis hotel creation
    /// </summary>
    public class AvisHotelCreateDTOValidator : AbstractValidator<AvisHotelCreateDTO>
    {
        public AvisHotelCreateDTOValidator()
        {
            RuleFor(opt => opt.Description).NotEmpty();
            RuleFor(opt => opt.Note).NotEmpty();
            RuleFor(opt => opt.HotelId).NotEmpty();
        }
    }
}
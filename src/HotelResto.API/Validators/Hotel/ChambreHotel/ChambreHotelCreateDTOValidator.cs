using System.Data;
using FluentValidation;
using HotelResto.API.Dto.Hotel.ChambreHotel;

namespace HotelResto.API.Validators.Hotel.ChambreHotel
{
    public class ChambreHotelCreateDTOValidator : AbstractValidator<ChambreHotelCreateDTO>
    {
        public ChambreHotelCreateDTOValidator()
        {
            RuleFor(x => x.Description).NotEmpty();
            RuleFor(x => x.Size).NotEmpty();
            RuleFor(x => x.HotelId).NotEmpty();
        }
    }
}
using FluentValidation;
using HotelResto.API.Dto.ZoneReservation;

namespace HotelResto.API.Validators.ZoneReservation
{
    public class ZoneReservationPossibleHotelNameDTOValidator : AbstractValidator<ZoneReservationPossibleHotelNameDTO>
    {
        public ZoneReservationPossibleHotelNameDTOValidator()
        {
            RuleFor(x => x.Arrive).NotEmpty();
            RuleFor(x => x.Depart).NotEmpty();
            RuleFor(x => x.HotelName).NotEmpty();
            RuleFor(x => x.NbrChambre).NotEmpty();
            RuleFor(x => x.NbrPersonne).NotEmpty();
        }
    }
}
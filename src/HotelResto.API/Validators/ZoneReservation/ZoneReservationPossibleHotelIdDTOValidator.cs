using FluentValidation;
using HotelResto.API.Dto.ZoneReservation;

namespace HotelResto.API.Validators.ZoneReservation
{
    public class ZoneReservationPossibleHotelIdDTOValidator : AbstractValidator<ZoneReservationPossibleHotelIdDTO>
    {
        public ZoneReservationPossibleHotelIdDTOValidator()
        {
            RuleFor(x => x.Arrive).NotEmpty();
            RuleFor(x => x.Depart).NotEmpty();
            RuleFor(x => x.HotelId).NotEmpty();
            RuleFor(x => x.NbrChambre).NotEmpty();
            RuleFor(x => x.NbrPersonne).NotEmpty();
        }
    }
}
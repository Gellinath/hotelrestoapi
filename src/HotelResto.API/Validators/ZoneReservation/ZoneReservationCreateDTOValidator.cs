using FluentValidation;
using HotelResto.API.Dto.ZoneReservation;

namespace HotelResto.API.Validators.ZoneReservation
{
    public class ZoneReservationCreateDTOValidator : AbstractValidator<ZoneReservationCreateDTO>
    {
        public ZoneReservationCreateDTOValidator()
        {
            RuleFor(x => x.HotelId).NotEmpty();
            RuleFor(x => x.Arrive).NotEmpty();
            RuleFor(x => x.Depart).NotEmpty();
            RuleFor(x => x.NbrPersonne).NotEmpty();
            RuleFor(x => x.NbrChambre).NotEmpty();
        }
    }
}
using FluentValidation;
using HotelResto.API.Dto.ZoneReservation;

namespace HotelResto.API.Validators.ZoneReservation
{
    public class ZoneReservationPossibleHotelVilleDTOValidator : AbstractValidator<ZoneReservationPossibleHotelVilleDTO>
    {
        public ZoneReservationPossibleHotelVilleDTOValidator()
        {
            RuleFor(x => x.Arrive).NotEmpty();
            RuleFor(x => x.Depart).NotEmpty();
            RuleFor(x => x.HotelVille).NotEmpty();
            RuleFor(x => x.NbrChambre).NotEmpty();
            RuleFor(x => x.NbrPersonne).NotEmpty();
        }
    }
}
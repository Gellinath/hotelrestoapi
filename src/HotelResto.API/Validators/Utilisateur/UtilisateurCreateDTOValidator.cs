using FluentValidation;
using HotelResto.API.Dto.Utilisateur;

namespace HotelResto.API.Validators.Utilisateur
{
    public class UtilisateurCreateDTOValidator : AbstractValidator<UtilisateurCreateDTO>
    {
        public UtilisateurCreateDTOValidator()
        {
            RuleFor(x => x.Nom).NotEmpty();
            RuleFor(x => x.Prenom).NotEmpty();
            RuleFor(x => x.Telephone).NotEmpty();
            RuleFor(x => x.Email).NotEmpty();
            RuleFor(x => x.Login).NotEmpty();
            RuleFor(x => x.MDP).NotEmpty();
        }
    }
}
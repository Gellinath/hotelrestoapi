using FluentValidation;
using HotelResto.API.Dto.Utilisateur;

namespace HotelResto.API.Validators.Utilisateur
{
    /// <summary>
    /// Utilisateur login DTO validator
    /// </summary>
    public class UtlisateurLoginDTOValidator : AbstractValidator<UtilisateurLoginDTO>
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public UtlisateurLoginDTOValidator()
        {
            RuleFor(x => x.Login).NotEmpty();
            RuleFor(x => x.MDP).NotEmpty();
        }
    }
}
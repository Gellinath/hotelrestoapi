using System.Collections;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;
using HotelResto.API.Dto.Restaurant.AvisRestaurant;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Services.Restaurant;
using HotelResto.API.Infrastructure.Services.Restaurant.AvisRestaurant;
using HotelResto.API.Utils.API;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HotelResto.API.Infrastructure.Services.Utilisateurs;

namespace HotelResto.API.Controllers.Restaurant.AvisRestaurant
{
    [ApiController]
    public class AvisRestaurantController : ControllerBase
    {
        private readonly IAvisRestaurantService _avisRestaurantService;
        private readonly IRestaurantService _restaurantService;
        private readonly IUtilisateurService _utilisateurService;
        private readonly IMapper _mapper;

        public AvisRestaurantController(IAvisRestaurantService avisRestaurantService, IRestaurantService restaurantService, IUtilisateurService utilisateurService, IMapper mapper)
        {
            _avisRestaurantService = avisRestaurantService ?? throw new ArgumentNullException(nameof(avisRestaurantService));
            _restaurantService = restaurantService ?? throw new ArgumentNullException(nameof(restaurantService));
            _utilisateurService = utilisateurService ?? throw new ArgumentNullException(nameof(utilisateurService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet, Route(UrlUtils.RESTAURANT_RESSOURCE.AVIS_RESTAURANT), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<AvisRestaurantDTO>), (int)HttpStatusCode.OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAvisRestaurants([FromRoute] int restaurantId)
        {
            var avisRestaurants = await _avisRestaurantService.GetAllAvisRestaurant(restaurantId);

            return Ok(_mapper.Map<IEnumerable<AvisRestaurantDTO>>(avisRestaurants));
        }

        [HttpGet, Route(UrlUtils.RESTAURANT_RESSOURCE.AVIS_RESTAURANT_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(AvisRestaurantDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAvisRestaurantById([FromRoute] int avisRestaurantId, int restaurantId)
        {
            var avisRestaurant = await _avisRestaurantService.GetAvisRestaurant(avisRestaurantId, restaurantId);

            if (avisRestaurant is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<AvisRestaurantDTO>(avisRestaurant));
            
        }

        [HttpPost, Route(UrlUtils.RESTAURANT_RESSOURCE.AVIS_RESTAURANT), Produces("application/json")]
        [ProducesResponseType(typeof(AvisRestaurantDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostAvisRestaurant([FromBody] AvisRestaurantCreateDTO AvisRestaurantCreateDTO, int restaurantId, int userId)
        {
            var user = await _utilisateurService.GetUtilisateur(userId);
            if(user == null)
            {
                return NotFound();
            }

            else if(!user.Connected)
            {
                return Unauthorized();
            }

            var avisRestaurant = await _avisRestaurantService.AddAvisRestaurant(_mapper.Map<AvisRestaurantEntity>(AvisRestaurantCreateDTO), restaurantId);
            
            //application de la nouvelle moyenne de note à l'hotel
            var listAvis = (IEnumerable<int>)await _avisRestaurantService.GetAllNotesAvisRestaurant(restaurantId);
            var moyAvis = listAvis.Average();

            //await _RestaurantService.UpdateMoyRestaurant(moyAvis, restaurantId);
            var restaurant = await _restaurantService.GetRestaurant(restaurantId);
            restaurant.MoyNote = moyAvis;
            await _restaurantService.PutRestaurant(restaurant);


            return CreatedAtAction(
                nameof(AvisRestaurantController.GetAvisRestaurantById),
                "AvisRestaurant", new {restaurantId = avisRestaurant.RestaurantId, avisRestaurantId = avisRestaurant.Id }, _mapper.Map<AvisRestaurantDTO>(avisRestaurant));
            
        }
    }
}
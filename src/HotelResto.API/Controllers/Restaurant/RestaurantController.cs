using System.Net;
using System;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System.Collections.Generic;
using HotelResto.API.Dto.Restaurant;
using HotelResto.API.Utils.API;
using Microsoft.AspNetCore.Http;
using HotelResto.API.Infrastructure.Services.Restaurant;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Services.Restaurant.AvisRestaurant;
using System.Collections;
using System.Linq;
using HotelResto.API.Dto.Restaurant.AvisRestaurant;
using HotelResto.API.Infrastructure.Services.Utilisateurs;

namespace HotelResto.API.Controllers.Restaurant
{
    [ApiController]
    public class RestaurantController : ControllerBase
    {
        private readonly IRestaurantService _restaurantService;
        private readonly IAvisRestaurantService _avisRestaurantService;
        private readonly IUtilisateurService _utilisateurService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="avisRestaurantService"></param>
        /// <param name="restaurantService"></param>
        /// <param name="mapper"></param>
        public RestaurantController(IAvisRestaurantService avisRestaurantService, IRestaurantService restaurantService, IUtilisateurService utilisateurService ,IMapper mapper)
        {
            _restaurantService = restaurantService ?? throw new ArgumentNullException(nameof(restaurantService));
            _avisRestaurantService = avisRestaurantService ?? throw new ArgumentNullException(nameof(avisRestaurantService));
            _utilisateurService = utilisateurService ?? throw new ArgumentNullException(nameof(utilisateurService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all restaurants
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.RESTAURANT_RESSOURCE.RESTAURANT), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RestaurantDTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetRestaurants()
        {
            var restaurants = await _restaurantService.GetAllRestaurant();
            return Ok(_mapper.Map<IEnumerable<RestaurantDTO>>(restaurants));
        }

        /// <summary>
        /// Get top 5 of restaurants
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.RESTAURANT_RESSOURCE.RESTAURANT_TOP5), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<RestaurantTop5DTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetTopRestaurants()
        {
            var restaurants = await _restaurantService.GetTop5Restaurant();
            var top5Restaurants = new List<RestaurantTop5DTO>();
            
            foreach (var item in restaurants)
            {
                var NbrAvis = (ICollection)await _avisRestaurantService.GetAllAvisRestaurant(item.Id);

                var count = 0;
                foreach(var e in NbrAvis)
                {
                    count++;
                    
                }

                var restaurantTop = new RestaurantTop5DTO();
                restaurantTop.Id = item.Id;
                restaurantTop.Name = item.Name;
                restaurantTop.MoyNote = item.MoyNote;
                restaurantTop.Description = item.Description;
                restaurantTop.Image = item.Image;
                restaurantTop.Telephone = item.Telephone;
                restaurantTop.Email = item.Email;
                restaurantTop.NumRue = item.NumRue;
                restaurantTop.NameRue = item.NameRue;
                restaurantTop.CP = item.CP;
                restaurantTop.Ville = item.Ville;
                restaurantTop.nbAvis = count;
                top5Restaurants.Add(restaurantTop);

            }

            return Ok(_mapper.Map<IEnumerable<RestaurantTop5DTO>>(top5Restaurants));
        }

        /// <summary>
        /// Get restaurant by id
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.RESTAURANT_RESSOURCE.RESTAURANT_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(RestaurantAndAvisDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRestaurantById([FromRoute] int restaurantId)
        {
            var restaurant = await _restaurantService.GetRestaurant(restaurantId);

            var restaurantAndAvis = new RestaurantAndAvisDTO();
            restaurantAndAvis.Id = restaurant.Id;
            restaurantAndAvis.Image = restaurant.Image;
            restaurantAndAvis.Name = restaurant.Name;
            restaurantAndAvis.Description = restaurant.Description;
            restaurantAndAvis.Telephone = restaurant.Telephone;
            restaurantAndAvis.Email = restaurant.Email;
            restaurantAndAvis.NumRue = restaurant.NumRue;
            restaurantAndAvis.NameRue = restaurant.NameRue;
            restaurantAndAvis.CP = restaurant.CP;
            restaurantAndAvis.Ville = restaurant.Ville;
            restaurantAndAvis.MoyNote = restaurant.MoyNote;
            restaurantAndAvis.Avis = await _avisRestaurantService.GetAllAvisRestaurant(restaurant.Id);

            if (restaurant is null)
            {
                return NotFound();
            }

            return Ok(restaurantAndAvis);
            
        }

        /// <summary>
        /// Get all restaurants by ville
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.RESTAURANT_RESSOURCE.RESTAURANT_BY_VILLE), Produces("application/json")]
        [ProducesResponseType(typeof(RestaurantDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRestaurantByVille([FromRoute] string ville)
        {
            var restaurant = await _restaurantService.GetRestaurantByVille(ville);

            if (restaurant is null || !restaurant.Any())
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<RestaurantDTO>>(restaurant));
            
        }

        /// <summary>
        /// Get all restaurants by name
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.RESTAURANT_RESSOURCE.RESTAURANT_BY_NAME), Produces("application/json")]
        [ProducesResponseType(typeof(RestaurantDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetRestaurantByName([FromRoute] string name)
        {
            var restaurant = await _restaurantService.GetRestaurantByName(name);

            if (restaurant is null || !restaurant.Any())
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<RestaurantDTO>>(restaurant));
            
        }

        /// <summary>
        /// Add restaurant
        /// </summary>
        /// <param name="RestaurantCreateDTO"></param>
        /// <returns></returns>
        [HttpPost, Route(UrlUtils.RESTAURANT_RESSOURCE.RESTAURANT), Produces("application/json")]
        [ProducesResponseType(typeof(RestaurantDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> PostRestaurant([FromBody] RestaurantCreateDTO RestaurantCreateDTO, int userId)
        {
            var user = await _utilisateurService.GetUtilisateur(userId);
            if(user == null)
            {
                return NotFound();
            }

            else if(!user.Connected)
            {
                return Unauthorized();
            }

            var restaurant = await _restaurantService.AddRestaurant(_mapper.Map<RestaurantEntity>(RestaurantCreateDTO));

            return CreatedAtAction(
                nameof(RestaurantController.GetRestaurantById),
                "Restaurant", new {restaurantId = restaurant.Id }, _mapper.Map<RestaurantDTO>(restaurant));
            
        }
    }
}
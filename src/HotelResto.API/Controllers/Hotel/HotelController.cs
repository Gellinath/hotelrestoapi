using System.Collections;
using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using HotelResto.API.Dto.Hotel;
using HotelResto.API.Infrastructure.Services.Hotel;
using HotelResto.API.Utils.API;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Services.Hotel.Avis;
using System.Linq;
using HotelResto.API.Infrastructure.Services.Utilisateurs;
using HotelResto.API.Infrastructure.Services.Hotel.ChambreHotel;

namespace HotelResto.API.Controllers.Hotel
{
    /// <summary>
    /// Hotel controller
    /// </summary>
    [ApiController]
    public class HotelController : ControllerBase
    {
        private readonly IHotelService _hotelService;
        private readonly IAvisHotelService _avisHotelService;
        private readonly IUtilisateurService _utilisateurService;
        private readonly IChambreHotelService _chambreHotelService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="hotelService"></param>
        /// <param name="mapper"></param>
        public HotelController(IAvisHotelService avisHotelService,  IHotelService hotelService, IUtilisateurService utilisateurService, IChambreHotelService chambreHotelService, IMapper mapper)
        {
            _hotelService = hotelService ?? throw new ArgumentNullException(nameof(hotelService));
            _avisHotelService = avisHotelService ?? throw new ArgumentNullException(nameof(avisHotelService));
            _utilisateurService = utilisateurService ?? throw new ArgumentNullException(nameof(utilisateurService));
            _chambreHotelService = chambreHotelService ?? throw new ArgumentNullException(nameof(chambreHotelService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all hotels
        /// </summary>
        /// <returns>Hotel DTO list</returns>
        /// <response code="200">Returns the list of all hotels</response>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.HOTEL), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<HotelDTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetHotels()
        {
            var hotels = await _hotelService.GetAllHotels();
            foreach (var hotel in hotels)
            {
                var nbChambre = await _chambreHotelService.GetAllChambreHotel(hotel.Id);
                hotel.RoomsAvailable = nbChambre.Count();
            }
            return Ok(_mapper.Map<IEnumerable<HotelDTO>>(hotels));
        }


        /// <summary>
        /// Get top 5 hotel
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.HOTEL_TOP5), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<HotelTop5DTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetTopHotel()
        {
            var hotels = await _hotelService.GetTop5Hotel();
            var top5Hotels = new List<HotelTop5DTO>();
            
            foreach (var item in hotels)
            {
                var NbrAvis = (ICollection)await _avisHotelService.GetAllAvisHotel(item.Id);

                var count = 0;
                foreach(var e in NbrAvis)
                {
                    count++;
                    
                }

                var hotelTop = new HotelTop5DTO();
                hotelTop.Id = item.Id;
                hotelTop.Name = item.Name;
                hotelTop.Description = item.Description;
                hotelTop.Image = item.Image;
                hotelTop.Email = item.Email;
                hotelTop.NumRue = item.NumRue;
                hotelTop.NameRue = item.NameRue;
                hotelTop.CP = item.CP;
                hotelTop.Ville = item.Ville;
                hotelTop.nbAvis = count;
                hotelTop.MoyNote = item.MoyNote;
                top5Hotels.Add(hotelTop);
            }

            return Ok(_mapper.Map<IEnumerable<HotelTop5DTO>>(top5Hotels));
        }

        /// <summary>
        /// Get all hotel by ville
        /// </summary>
        /// <param name="hotelId"></param>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.HOTEL_BY_VILLE), Produces("application/json")]
        [ProducesResponseType(typeof(HotelDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetHotelByVille([FromRoute] string ville)
        {
            var hotel = await _hotelService.GetHotelByVille(ville);

            if (hotel is null || !hotel.Any())
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<HotelDTO>>(hotel));
            
        }

        /// <summary>
        /// Get all hotels by name
        /// </summary>
        /// <param name="restaurantId"></param>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.HOTEL_BY_NAME), Produces("application/json")]
        [ProducesResponseType(typeof(HotelDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetHotelByName([FromRoute] string name)
        {
            var hotel = await _hotelService.GetHotelByName(name);

            if (hotel is null || !hotel.Any())
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<HotelDTO>>(hotel));
            
        }

        /// <summary>
        /// Get a hotel according to an identifier
        /// </summary>
        /// <param name="hotelId">Hotel unique identifier</param>
        /// <returns>HotelDTO</returns>
        /// <response code="200">Returns a hotel by the id</response>
        /// <response code="404">Cannot find the hotel</response>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.HOTEL_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(HotelAndAvisDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetHotelById([FromRoute] int hotelId)
        {
            var hotel = await _hotelService.GetHotelById(hotelId);

            var nbChambre = await _chambreHotelService.GetAllChambreHotel(hotelId);
            hotel.RoomsAvailable = nbChambre.Count();
            
            var hotelAvis = new HotelAndAvisDTO();
            hotelAvis.Id = hotel.Id;
            hotelAvis.Name = hotel.Name;
            hotelAvis.Description = hotel.Description;
            hotelAvis.NumRue = hotel.NumRue;
            hotelAvis.NameRue = hotel.NameRue;
            hotelAvis.CP = hotel.CP;
            hotelAvis.Ville = hotel.Ville;
            hotelAvis.Email = hotel.Email;
            hotelAvis.Image = hotel.Image;
            hotelAvis.RoomsAvailable = hotel.RoomsAvailable;
            hotelAvis.MoyNote = hotel.MoyNote;
            hotelAvis.AvisHotel = await _avisHotelService.GetAllAvisHotel(hotel.Id);

            if (hotel is null)
            {
                return NotFound();
            }

            return Ok(hotelAvis);
        }

        /// <summary>
        /// Returns a created hotel
        /// </summary>
        /// <param name="hotelCreateDTO">the created hotel</param>
        /// <returns>HotelDTO</returns>
        /// <response code="201">Returns the created hotel</response>
        /// <response code="400">If the hotel is null</response>
        [HttpPost, Route(UrlUtils.HOTEL_RESSOURCE.HOTEL), Produces("application/json")]
        [ProducesResponseType(typeof(HotelDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostHotel([FromBody] HotelCreateDTO hotelCreateDTO, int userId)
        {
            var user = await _utilisateurService.GetUtilisateur(userId);
            if(user == null)
            {
                return NotFound();
            }

            else if(!user.Connected)
            {
                return Unauthorized();
            }

            var hotel = await _hotelService.AddHotel(_mapper.Map<HotelEntity>(hotelCreateDTO));

            return CreatedAtAction(
                nameof(HotelController.GetHotelById),
                "Hotel", new { hotelId = hotel.Id }, _mapper.Map<HotelDTO>(hotel));

        }
    }
}
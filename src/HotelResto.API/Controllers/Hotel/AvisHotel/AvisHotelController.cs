using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using HotelResto.API.Dto.Hotel.AvisHotel;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Services.Hotel;
using HotelResto.API.Infrastructure.Services.Hotel.Avis;
using HotelResto.API.Infrastructure.Services.Utilisateurs;
using HotelResto.API.Utils.API;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HotelResto.API.Controllers.Hotel.Avis
{
    /// <summary>
    /// Avis hotel controller
    /// </summary>
    [ApiController]
    public class AvisHotelController : ControllerBase
    {
        private readonly IAvisHotelService _avisHotelService;
        private readonly IHotelService _hotelService;
        private readonly IUtilisateurService _utilisateurService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="avisHotelService"></param>
        /// <param name="mapper"></param>
        public AvisHotelController(IHotelService hotelService, IAvisHotelService avisHotelService, IUtilisateurService utilisateurService, IMapper mapper)
        {
            _avisHotelService = avisHotelService ?? throw new ArgumentNullException(nameof(avisHotelService));
            _hotelService = hotelService ?? throw new ArgumentNullException(nameof(hotelService));
            _utilisateurService = utilisateurService ?? throw new ArgumentNullException(nameof(utilisateurService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all avis hotels
        /// </summary>
        /// <returns>Avis hotel DTO list</returns>
        /// <response code="200">Returns the list of all avis hotels</response>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.AVIS_HOTEL), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<AvisHotelDTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetAvisHotels([FromRoute] int hotelId)
        {
            var avisHotels = await _avisHotelService.GetAllAvisHotel(hotelId);

            if (avisHotels is null || !avisHotels.Any())
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<AvisHotelDTO>>(avisHotels));
        }

        /// <summary>
        /// Get an avis hotel according to an identifier
        /// </summary>
        /// <param name="avisHotelId">avis hotel unique identifier</param>
        /// <returns>AvisHotelDTO</returns>
        /// <response code="200">Returns avis hotel by the id</response>
        /// <response code="404">Cannot find the avis hotel</response>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.AVIS_HOTEL_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(AvisHotelDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetAvis([FromRoute] int avisHotelId, int hotelId)
        {
            var avisHotel = await _avisHotelService.GetAvisHotelById(avisHotelId, hotelId);

            if (avisHotel is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<AvisHotelDTO>(avisHotel));
        }

        /// <summary>
        /// Returns a created avis hotel
        /// </summary>
        /// <param name="hotelCreateDTO">the created avis hotel</param>
        /// <returns>AvisHotelDTO</returns>
        /// <response code="201">Returns the created avis hotel</response>
        /// <response code="400">If the avis hotel is null</response>
        [HttpPost, Route(UrlUtils.HOTEL_RESSOURCE.AVIS_HOTEL), Produces("application/json")]
        [ProducesResponseType(typeof(AvisHotelDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostHotel([FromBody] AvisHotelCreateDTO avisHotelCreateDTO, int hotelId, int userId)
        {
            var user = await _utilisateurService.GetUtilisateur(userId);
            if(user == null)
            {
                return NotFound();
            }

            else if(!user.Connected)
            {
                return Unauthorized();
            }

            var avisHotel = await _avisHotelService.AddAvisHotel(_mapper.Map<AvisHotelEntity>(avisHotelCreateDTO), hotelId);

            //application de la nouvelle moyenne de note à l'hotel
            var listAvis = (IEnumerable<int>)await _avisHotelService.GetAllNotesAvisHotel(hotelId);
            var moyAvis = listAvis.Average();

            //await _RestaurantService.UpdateMoyRestaurant(moyAvis, restaurantId);
            var hotel = await _hotelService.GetHotelById(hotelId);
            hotel.MoyNote = (int)moyAvis;
            await _hotelService.PutHotel(hotel);

            return CreatedAtAction(
                nameof(AvisHotelController.GetAvis),
                "AvisHotel", new {hotelId = avisHotel.HotelId, avisHotelId = avisHotel.Id }, _mapper.Map<AvisHotelDTO>(avisHotel));
        }
    }
}
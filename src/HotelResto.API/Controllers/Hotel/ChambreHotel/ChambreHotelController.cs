using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AutoMapper;
using HotelResto.API.Dto.Hotel.ChambreHotel;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Services.Hotel.ChambreHotel;
using HotelResto.API.Infrastructure.Services.Utilisateurs;
using HotelResto.API.Utils.API;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HotelResto.API.Controllers.Hotel.ChambreHotel
{
    /// <summary>
    /// Chambre hotel controller
    /// </summary>
    [ApiController]
    public class ChambreHotelController : ControllerBase
    {
        private readonly IChambreHotelService _chambreHotelService;
        private readonly IUtilisateurService _utilisateurService;
        private readonly IMapper _mapper;

        public ChambreHotelController(IChambreHotelService chambreHotelService, IUtilisateurService utilisateurService, IMapper mapper)
        {
            _chambreHotelService = chambreHotelService ?? throw new ArgumentNullException(nameof(chambreHotelService));
            _utilisateurService = utilisateurService ?? throw new ArgumentNullException(nameof(utilisateurService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all chambre hotels
        /// </summary>
        /// <returns>Chambre Hotel DTO list</returns>
        /// <response code="200">Returns the list of all chambre hotels</response>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.CHAMBRE), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<ChambreHotelDTO>), (int)HttpStatusCode.OK)]
        public async Task<IActionResult> GetChambresHotel([FromRoute] int hotelId)
        {
            var chambre = await _chambreHotelService.GetAllChambreHotel(hotelId);
            return Ok(_mapper.Map<IEnumerable<ChambreHotelDTO>>(chambre));
        }

        /// <summary>
        /// Get a chambre hotel according to an identifier
        /// </summary>
        /// <param name="chambreId">Chambre unique identifier</param>
        /// <param name="hotelId">Hotel unique identifier</param>
        /// <returns>ChambreHotelDTO</returns>
        /// <response code="200">Returns a chambre hotel by the id</response>
        /// <response code="404">Cannot find the chambre hotel</response>
        [HttpGet, Route(UrlUtils.HOTEL_RESSOURCE.CHAMBRE_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(ChambreHotelDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetChambreHotelById([FromRoute]int chambreId, int hotelId)
        {
            var chambre = await _chambreHotelService.GetChambreHotelById(chambreId, hotelId);

            if (chambre is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ChambreHotelDTO>(chambre));
        }

        /// <summary>
        /// Returns a created chambre hotel
        /// </summary>
        /// <param name="chambreHotelCreateDTO">the created hotel</param>
        /// <returns>ChambreHotelDTO</returns>
        /// <response code="201">Returns the created chambre hotel</response>
        /// <response code="400">If the chambre hotel is null</response>
        [HttpPost, Route(UrlUtils.HOTEL_RESSOURCE.CHAMBRE), Produces("application/json")]
        [ProducesResponseType(typeof(ChambreHotelDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostChambreHotel([FromBody] ChambreHotelCreateDTO chambreHotelCreateDTO, int hotelId, int userId)
        {
            var user = await _utilisateurService.GetUtilisateur(userId);
            if(user == null)
            {
                return NotFound();
            }

            else if(!user.Connected)
            {
                return Unauthorized();
            }

            var chambre = await _chambreHotelService.AddChambreHotel(_mapper.Map<ChambreHotelEntity>(chambreHotelCreateDTO), hotelId);

            return CreatedAtAction(
                nameof(ChambreHotelController.GetChambreHotelById),
                "ChambreHotel", new {hotelId = chambre.HotelId, chambreId = chambre.Id }, _mapper.Map<ChambreHotelDTO>(chambre));

        }
    }
}
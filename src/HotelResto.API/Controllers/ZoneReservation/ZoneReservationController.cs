using System.Xml.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using HotelResto.API.Dto.ZoneReservation;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Services.Hotel.ChambreHotel;
using HotelResto.API.Infrastructure.Services.ZoneReservation;
using HotelResto.API.Utils.API;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using HotelResto.API.Infrastructure.Services.Hotel;
using HotelResto.API.Dto.Hotel.ChambreHotel;
using HotelResto.API.Infrastructure.Services.Utilisateurs;

namespace HotelResto.API.Controllers.ZoneReservation
{
    public class ZoneReservationController : ControllerBase
    {
        /// <summary>
        /// récupération des interfaces 
        /// </summary>
        private readonly IZoneReservationService _zoneReservationService;
        private readonly IHotelService _hotelService;
        private readonly IChambreHotelService _chambreHotelService;
        private readonly IUtilisateurService _utilisateurService;
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructeur
        /// </summary>
        /// <param name="zoneReservationService"></param>
        /// <param name="chambreHotelService"></param>
        /// <param name="hotelService"></param>
        /// <param name="mapper"></param>
        public ZoneReservationController(IUtilisateurService utilisateurService, IZoneReservationService zoneReservationService, IChambreHotelService chambreHotelService,IHotelService hotelService, IMapper mapper)
        {
            _utilisateurService = utilisateurService ?? throw new ArgumentNullException(nameof(utilisateurService));
            _zoneReservationService = zoneReservationService ?? throw new ArgumentNullException(nameof(zoneReservationService));
            _hotelService = hotelService ?? throw new ArgumentNullException(nameof(hotelService));
            _chambreHotelService = chambreHotelService ?? throw new ArgumentNullException(nameof(zoneReservationService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        /// <summary>
        /// Get all zoneReservation for a user by user id
        /// </summary>
        /// <param name="utilisateurId"></param>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ZONE_RESERVATION_USER), Produces("application/json")]
        [ProducesResponseType(typeof(IEnumerable<ZoneReservationDTO>), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetZoneReservationOfUserId([FromRoute] int utilisateurId)
        {
            var zoneReservation = await _zoneReservationService.GetAllByUserIdZoneReservation(utilisateurId);

            if (zoneReservation is null|| !zoneReservation.Any())
            {
                return NotFound();
            }

            return Ok(_mapper.Map<IEnumerable<ZoneReservationDTO>>(zoneReservation));
            
        }

        /// <summary>
        /// Get ZoneReservation unique of a user by user id and zoneReservation id
        /// </summary>
        /// <param name="utilisateurId"></param>
        /// <param name="zone_reservationId"></param>
        /// <returns></returns>
        [HttpGet, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ZONE_RESERVATION_USER_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(ZoneReservationDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetZoneReservationByIdOfUserId([FromRoute] int utilisateurId, int zone_reservationId)
        {
            var zoneReservation = await _zoneReservationService.GetZoneReservationByIdAndUserId(zone_reservationId, utilisateurId);

            if (zoneReservation is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ZoneReservationDTO>(zoneReservation));
            
        }

        /// <summary>
        /// Get all chambres disponibles by hotel id and dates arrive and depart
        /// </summary>
        /// <param name="zoneReservationPossibleHotelIdDTO"></param>
        /// <returns></returns>
        [HttpPost, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ALL_RESERVATION_HOTEL_ID), Produces("application/json")]
        [ProducesResponseType(typeof(List<List<ChambreHotelDTO>>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetZoneReservationPossibleByHotelId([FromBody] ZoneReservationPossibleHotelIdDTO zoneReservationPossibleHotelIdDTO)
        {

            var ChambreHotel = (List<ChambreHotelEntity>)await _chambreHotelService.GetAllChambreHotel(zoneReservationPossibleHotelIdDTO.HotelId);

            var ChambreHotelLibre = new List<List<ChambreHotelEntity>>();

            var ChambreReserve = await _zoneReservationService
            .GetAllByHotelIdAndDateZoneReservation(zoneReservationPossibleHotelIdDTO.HotelId, zoneReservationPossibleHotelIdDTO.Arrive, zoneReservationPossibleHotelIdDTO.Depart);

            foreach (var item in ChambreReserve)
            {
                for (int i = 0; i<item.NbrChambre; i++){

                    //ajout des chambres a enlever de ChambreHotel
                    var chambre = ChambreHotel
                    .Where(ch => ch.Size == item.NbrPersonne/item.NbrChambre).FirstOrDefault();

                    if (chambre is null)
                    {
                        chambre = ChambreHotel
                        .Where(ch => ch.Size > item.NbrPersonne/item.NbrChambre).FirstOrDefault();
                    }

                    ChambreHotel.Remove(chambre);

                    i++;
                }
                
                
            }

            for (int z = 0; z<ChambreHotel.Count(); z++)
                {
                    var reserve = new List<ChambreHotelEntity>();

                    for (int p = 0; p<zoneReservationPossibleHotelIdDTO.NbrChambre; p++)
                    {
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == zoneReservationPossibleHotelIdDTO.NbrPersonne/zoneReservationPossibleHotelIdDTO.NbrChambre).FirstOrDefault();

                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > zoneReservationPossibleHotelIdDTO.NbrPersonne/zoneReservationPossibleHotelIdDTO.NbrChambre).FirstOrDefault();
                        }
                        if (chambre != null)
                        {
                            reserve.Add(chambre);
                        }
                        ChambreHotel.Remove(chambre);
                    }
                    if (reserve != null && reserve.Any())
                    {
                        ChambreHotelLibre.Add(reserve);
                    }
                }

            if (ChambreHotelLibre is null|| !ChambreHotelLibre.Any())
            {
                return NotFound();
            }else{

                return Ok(_mapper.Map<List<List<ChambreHotelDTO>>>(ChambreHotelLibre));
            }
            
        }

        /// <summary>
        /// Get all chambres disponibles by hotel ville and dates arrive and depart
        /// </summary>
        /// <param name="zoneReservationPossibleHotelVilleDTO"></param>
        /// <returns></returns>
        [HttpPost, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ALL_RESERVATION_HOTEL_VILLE), Produces("application/json")]
        [ProducesResponseType(typeof(List<List<ChambreHotelDTO>>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetZoneReservationPossibleByHotelVille([FromBody] ZoneReservationPossibleHotelVilleDTO zoneReservationPossibleHotelVilleDTO)
        {

            var hotels = await _hotelService.GetHotelByVille(zoneReservationPossibleHotelVilleDTO.HotelVille);
            var ChambreHotelLibre = new List<List<ChambreHotelEntity>>();

            foreach (var hotel in hotels)
            {
                var ChambreHotel = (List<ChambreHotelEntity>)await _chambreHotelService.GetAllChambreHotel(hotel.Id);

                var ChambreReserve = await _zoneReservationService
                .GetAllByHotelIdAndDateZoneReservation(hotel.Id, zoneReservationPossibleHotelVilleDTO.Arrive, zoneReservationPossibleHotelVilleDTO.Depart);

                foreach (var item in ChambreReserve)
                {
                    for (int i = 0; i<item.NbrChambre; i++){

                        //ajout des chambres a enlever de ChambreHotel
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == item.NbrPersonne/item.NbrChambre).FirstOrDefault();

                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > item.NbrPersonne/item.NbrChambre).FirstOrDefault();
                        }

                        ChambreHotel.Remove(chambre);

                        i++;
                    }
                }

                for (int z = 0; z<ChambreHotel.Count(); z++)
                {
                    var reserve = new List<ChambreHotelEntity>();

                    for (int p = 0; p<zoneReservationPossibleHotelVilleDTO.NbrChambre; p++)
                    {
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == zoneReservationPossibleHotelVilleDTO.NbrPersonne/zoneReservationPossibleHotelVilleDTO.NbrChambre).FirstOrDefault();

                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > zoneReservationPossibleHotelVilleDTO.NbrPersonne/zoneReservationPossibleHotelVilleDTO.NbrChambre).FirstOrDefault();
                        }
                        if (chambre != null)
                        {
                            reserve.Add(chambre);
                        }
                        ChambreHotel.Remove(chambre);
                    }
                    if (reserve != null && reserve.Any())
                    {
                        ChambreHotelLibre.Add(reserve);
                    }
                }

            }

            if (ChambreHotelLibre is null|| !ChambreHotelLibre.Any())
            {
                return NotFound();
            }else{

                return Ok(_mapper.Map<List<List<ChambreHotelDTO>>>(ChambreHotelLibre));
            }
        }

        /// <summary>
        /// Get all chambres disponibles by hotel name and dates arrive and depart
        /// </summary>
        /// <param name="zoneReservationPossibleHotelNameDTO"></param>
        /// <returns></returns>
        [HttpPost, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ALL_RESERVATION_HOTEL_NAME), Produces("application/json")]
        [ProducesResponseType(typeof(List<List<ChambreHotelDTO>>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetZoneReservationPossibleByHotelName([FromBody] ZoneReservationPossibleHotelNameDTO zoneReservationPossibleHotelNameDTO)
        {

            var hotels = await _hotelService.GetHotelByName(zoneReservationPossibleHotelNameDTO.HotelName);
            var ChambreHotelLibre = new List<List<ChambreHotelEntity>>();

            foreach (var hotel in hotels)
            {
                var ChambreHotel = (List<ChambreHotelEntity>)await _chambreHotelService.GetAllChambreHotel(hotel.Id);

                var ChambreReserve = await _zoneReservationService
                .GetAllByHotelIdAndDateZoneReservation(hotel.Id, zoneReservationPossibleHotelNameDTO.Arrive, zoneReservationPossibleHotelNameDTO.Depart);

                foreach (var item in ChambreReserve)
                {
                    for (int i = 0; i<item.NbrChambre; i++)
                    {

                        //ajout des chambres a enlever de ChambreHotel
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == item.NbrPersonne/item.NbrChambre).FirstOrDefault();

                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > item.NbrPersonne/item.NbrChambre).FirstOrDefault();
                        }

                        ChambreHotel.Remove(chambre);

                        i++;
                    }
                }

                for (int z = 0; z<ChambreHotel.Count(); z++)
                {
                    var reserve = new List<ChambreHotelEntity>();

                    for (int p = 0; p<zoneReservationPossibleHotelNameDTO.NbrChambre; p++)
                    {
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == zoneReservationPossibleHotelNameDTO.NbrPersonne/zoneReservationPossibleHotelNameDTO.NbrChambre).FirstOrDefault();

                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > zoneReservationPossibleHotelNameDTO.NbrPersonne/zoneReservationPossibleHotelNameDTO.NbrChambre).FirstOrDefault();
                        }
                        if (chambre != null)
                        {
                            reserve.Add(chambre);
                        }
                        ChambreHotel.Remove(chambre);
                    }
                    if (reserve != null && reserve.Any())
                    {
                        ChambreHotelLibre.Add(reserve);
                    }
                }

            }

            if (ChambreHotelLibre is null|| !ChambreHotelLibre.Any())
            {
                return NotFound();
            }else{

                return Ok(_mapper.Map<List<List<ChambreHotelDTO>>>(ChambreHotelLibre));
            }
        }

        /// <summary>
        /// Get all chambres disponibles for all hotels by dates arrive and depart, nbrPersonnes and nbrChambres
        /// </summary>
        /// <param name="zoneReservationPossibleDTO"></param>
        /// <returns></returns>
        [HttpPost, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ALL_RESERVATION), Produces("application/json")]
        [ProducesResponseType(typeof(List<List<ChambreHotelDTO>>), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> GetZoneReservationPossibleForAllHotels([FromBody] ZoneReservationPossibleDTO zoneReservationPossibleDTO)
        {

            var hotels = await _hotelService.GetAllHotels();
            var ChambreHotelLibre = new List<List<ChambreHotelEntity>>();

            foreach (var hotel in hotels)
            {
                var ChambreHotel = (List<ChambreHotelEntity>)await _chambreHotelService.GetAllChambreHotel(hotel.Id);

                var ChambreReserve = await _zoneReservationService
                .GetAllByHotelIdAndDateZoneReservation(hotel.Id, zoneReservationPossibleDTO.Arrive, zoneReservationPossibleDTO.Depart);

                foreach (var item in ChambreReserve)
                {
                    for (int i = 0; i<item.NbrChambre; i++){

                        //ajout des chambres a enlever de ChambreHotel
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == item.NbrPersonne/item.NbrChambre).FirstOrDefault();

                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > item.NbrPersonne/item.NbrChambre).FirstOrDefault();
                        }

                        ChambreHotel.Remove(chambre);

                        i++;
                    }
                }

                for (int z = 0; z<ChambreHotel.Count(); z++)
                {
                    var reserve = new List<ChambreHotelEntity>();

                    for (int p = 0; p<zoneReservationPossibleDTO.NbrChambre; p++)
                    {
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == zoneReservationPossibleDTO.NbrPersonne/zoneReservationPossibleDTO.NbrChambre).FirstOrDefault();

                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > zoneReservationPossibleDTO.NbrPersonne/zoneReservationPossibleDTO.NbrChambre).FirstOrDefault();
                        }
                        if (chambre != null)
                        {
                            reserve.Add(chambre);
                        }
                        ChambreHotel.Remove(chambre);
                    }
                    if (reserve != null && reserve.Any())
                    {
                        ChambreHotelLibre.Add(reserve);
                    }
                }

            }

            if (ChambreHotelLibre is null|| !ChambreHotelLibre.Any())
            {
                return NotFound();
            }else{

                return Ok(_mapper.Map<List<List<ChambreHotelDTO>>>(ChambreHotelLibre));
            }
            
        }

        /// <summary>
        /// Add new zoneReservation for a user
        /// </summary>
        /// <param name="zoneReservationCreateDTO"></param>
        /// <param name="utilisateurId"></param>
        /// <returns></returns>
        [HttpPost, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ZONE_RESERVATION_USER), Produces("application/json")]
        [ProducesResponseType(typeof(ZoneReservationDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        public async Task<IActionResult> PostZoneReservationOfUser([FromBody] ZoneReservationCreateDTO zoneReservationCreateDTO,[FromRoute] int utilisateurId)
        {
            var user = await _utilisateurService.GetUtilisateur(utilisateurId);
            if(user == null)
            {
                return NotFound();
            }

            else if(!user.Connected)
            {
                return Unauthorized();
            }

            var zoneReservation = new ZoneReservationEntity();
            zoneReservation =_mapper.Map<ZoneReservationEntity>(zoneReservationCreateDTO);
            zoneReservation.UtilisateurId = utilisateurId;

            var ChambreHotel = (List<ChambreHotelEntity>)await _chambreHotelService.GetAllChambreHotel(zoneReservation.HotelId);
            var ChambreReserve = await _zoneReservationService.GetAllByHotelIdAndDateZoneReservation(zoneReservation.HotelId, zoneReservation.Arrive, zoneReservation.Depart);

            foreach (var item in ChambreReserve)
            {
                for (int i = 0; i<item.NbrChambre; i++){

                    //ajout des chambres a enlever de ChambreHotel
                    var chambre = ChambreHotel
                    .Where(ch => ch.Size == item.NbrPersonne/item.NbrChambre).FirstOrDefault();

                    if (chambre is null)
                    {
                        chambre = ChambreHotel
                        .Where(ch => ch.Size > item.NbrPersonne/item.NbrChambre).FirstOrDefault();
                    }

                    ChambreHotel.Remove(chambre);

                    i++;
                }
                
                
            }

            if (zoneReservation.NbrChambre > 1){
                for (int i = 0; i<zoneReservation.NbrChambre; i++){
                        var chambre = ChambreHotel
                        .Where(ch => ch.Size == zoneReservation.NbrPersonne/zoneReservation.NbrChambre).FirstOrDefault();
                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > zoneReservation.NbrPersonne/zoneReservation.NbrChambre).FirstOrDefault();
                        }

                        ChambreHotel.Remove(chambre);

                        i++;
                }
            }else{
                var chambre = ChambreHotel
                        .Where(ch => ch.Size == zoneReservation.NbrPersonne/zoneReservation.NbrChambre).FirstOrDefault();
                        if (chambre is null)
                        {
                            chambre = ChambreHotel
                            .Where(ch => ch.Size > zoneReservation.NbrPersonne/zoneReservation.NbrChambre).FirstOrDefault();
                        }
            }

            if (ChambreHotel is null|| !ChambreHotel.Any())
            {
                return Unauthorized();
            }

            zoneReservation = await _zoneReservationService.AddZoneReservation(_mapper.Map<ZoneReservationEntity>(zoneReservationCreateDTO), utilisateurId);

            return CreatedAtAction(
                nameof(ZoneReservationController.GetZoneReservationByIdOfUserId),
                "ZoneReservation", new {utilisateurId = zoneReservation.UtilisateurId, zone_reservationId = zoneReservation.Id},
                 _mapper.Map<ZoneReservationDTO>(zoneReservation));
            
        }

        /// <summary>
        /// Delete zoneReservation
        /// </summary>
        /// <param name="zone_reservationId"></param>
        /// <param name="utilisateurId"></param>
        /// <returns></returns>
        [HttpDelete, Route(UrlUtils.ZONE_RESERVATION_RESSOURCE.ZONE_RESERVATION_USER_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(ZoneReservationDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteZoneReservationOfUser([FromRoute] int zone_reservationId, int utilisateurId)
        {
            var user = await _utilisateurService.GetUtilisateur(utilisateurId);
            if(user == null)
            {
                return NotFound();
            }

            else if(!user.Connected)
            {
                return Unauthorized();
            }

            var zoneReservation = await _zoneReservationService.DeleteZoneReservation(zone_reservationId, utilisateurId);

            return Ok(_mapper.Map<ZoneReservationDTO>(zoneReservation));
            
        }
    }
}
using System;
using System.Threading.Tasks;
using AutoMapper;
using HotelResto.API.Dto.Utilisateur;
using HotelResto.API.Entities;
using HotelResto.API.Infrastructure.Services.Utilisateurs;
using HotelResto.API.Utils.API;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace HotelResto.API.Controllers.Utilisateur
{
    public class UtilisateurController : ControllerBase
    {
        private readonly IUtilisateurService _utilisateurService;
        private readonly IMapper _mapper;

        public UtilisateurController(IUtilisateurService utilisateurService, IMapper mapper)
        {
            _utilisateurService = utilisateurService ?? throw new ArgumentNullException(nameof(utilisateurService));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        [HttpGet, Route(UrlUtils.UTILISATEUR_RESSOURCE.UTILISATEUR_UNIQUE), Produces("application/json")]
        [ProducesResponseType(typeof(UtilisateurDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetUtilisateurById([FromRoute] int utilisateurId)
        {
            var utilisateur = await _utilisateurService.GetUtilisateur(utilisateurId);

            if (utilisateur is null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<UtilisateurDTO>(utilisateur));
            
        }

        [HttpPost, Route(UrlUtils.UTILISATEUR_RESSOURCE.UTILISATEUR), Produces("application/json")]
        [ProducesResponseType(typeof(UtilisateurDTO), StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> PostUtilisateur([FromBody] UtilisateurCreateDTO UtilisateurCreateDTO)
        {
            var utilisateur = await _utilisateurService.AddUtilisateur(_mapper.Map<UtilisateurEntity>(UtilisateurCreateDTO));

            return CreatedAtAction(
                nameof(UtilisateurController.GetUtilisateurById),
                "Utilisateur", new {utilisateurId = utilisateur.Id }, _mapper.Map<UtilisateurDTO>(utilisateur));
            
        }

        /// <summary>
        /// Login with a user login and password
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        [HttpPut, Route(UrlUtils.UTILISATEUR_RESSOURCE.UTILISATEUR_CONNECTION), Produces("application/json")]
        [ProducesResponseType(typeof(UtilisateurDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> Login([FromBody] UtilisateurLoginDTO user)
        {
            var utilisateur = await _utilisateurService.GetUtilisateurByLogin(user.Login);

            if (utilisateur.MDP == user.MDP)
            {
                utilisateur = await _utilisateurService.Login(utilisateur);
                return Ok(_mapper.Map<UtilisateurDTO>(utilisateur));
            }

            else if(utilisateur == null)
            {
                return NotFound(); 
            }

            else
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Disconnect a user
        /// </summary>
        /// <param name="utilisateurId"></param>
        /// <returns></returns>
        [HttpPut, Route(UrlUtils.UTILISATEUR_RESSOURCE.UTILISATEUR_UNIQUE_DISCONNECT), Produces("application/json")]
        [ProducesResponseType(typeof(UtilisateurDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Disconnect([FromRoute] int utilisateurId)
        {
            var utilisateur = await _utilisateurService.GetUtilisateur(utilisateurId);

            if(utilisateur == null)
            {
                return NotFound();
            }

            utilisateur = await _utilisateurService.Disconnect(utilisateur);
            return Ok(_mapper.Map<UtilisateurDTO>(utilisateur));
        }
    }
}
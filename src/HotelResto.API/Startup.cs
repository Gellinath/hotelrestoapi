using System;
using AutoMapper;
using FluentValidation.AspNetCore;
using HotelResto.API.Infrastructure.Repository.Restaurant;
using HotelResto.API.Infrastructure.Services.Restaurant;
using HotelResto.API.Infrastructure.Repository.Hotel;
using HotelResto.API.Infrastructure.Services.Hotel;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Converters;
using Pomelo.EntityFrameworkCore.MySql.Infrastructure;
using Swashbuckle.AspNetCore.Swagger;
using HotelResto.API.Infrastructure.Repository.Restaurant.AvisRestaurant;
using HotelResto.API.Infrastructure.Services.Restaurant.AvisRestaurant;
using HotelResto.API.Infrastructure.Repository.Hotel.AvisHotel;
using HotelResto.API.Infrastructure.Services.Hotel.Avis;
using HotelResto.API.Infrastructure.Filters;
using HotelResto.API.Infrastructure.Repository.Utilisateurs;
using HotelResto.API.Infrastructure.Services.Utilisateurs;
using HotelResto.API.Infrastructure.Repository.Hotel.ChambreHotel;
using HotelResto.API.Infrastructure.Services.Hotel.ChambreHotel;
using HotelResto.API.Infrastructure.Repository.ZoneReservation;
using HotelResto.API.Infrastructure.Services.ZoneReservation;

namespace HotelResto.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            //Db context configuration
            services.AddDbContextPool<Data.ApplicationDbContext>(
                options => options.UseMySql(Configuration.GetConnectionString("DefaultConnection"),
                mysqlOption =>
                {
                    mysqlOption.ServerVersion( new Version(10, 4), ServerType.MariaDb);
                    mysqlOption.EnableRetryOnFailure(
                        maxRetryCount: 15,
                        maxRetryDelay: TimeSpan.FromSeconds(30),
                        errorNumbersToAdd: null
                    );
                }
                )
            );
            
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                c.OrderActionsBy(api =>api.RelativePath);
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "My API", Version = "v1" });
                
                // Adds fluent validation rules to swagger

                c.AddFluentValidationRules();
            });

            

            services.AddControllers(options =>
            {
                options.Filters.Add(typeof(ValidatorActionFilter));
                options.Filters.Add(typeof(HttpGlobalExceptionFilter));
            })
            .AddNewtonsoftJson(
                option => option.SerializerSettings.ReferenceLoopHandling = 
                Newtonsoft.Json.ReferenceLoopHandling.Ignore
            )
            .AddNewtonsoftJson( option =>
            {
                option.SerializerSettings.Converters.Add(new StringEnumConverter());
            })
            .AddFluentValidation(fvc => fvc.RegisterValidatorsFromAssemblyContaining<Startup>());

            services.AddSwaggerGenNewtonsoftSupport();

            //Repository
            services.AddTransient<IRestaurantRepository, RestaurantRepository>();
            services.AddTransient<IAvisRestaurantRepository, AvisRestaurantRepository>();
            services.AddTransient<IHotelRepository, HotelRepository>();
            services.AddTransient<IAvisHotelRepository, AvisHotelRepository>();
            services.AddTransient<IUtilisateurRepository, UtilisateurRepository>();
            services.AddTransient<IChambreHotelRepository, ChambreHotelRepository>();
            services.AddTransient<IZoneReservationRepository, ZoneReservationRepository>();

            //services
            services.AddTransient<IRestaurantService, RestaurantService>();
            services.AddTransient<IAvisRestaurantService, AvisRestaurantService>();
            services.AddTransient<IHotelService, HotelService>();
            services.AddTransient<IAvisHotelService, AvisHotelService>();
            services.AddTransient<IUtilisateurService, UtilisateurService>();
            services.AddTransient<IChambreHotelService, ChambreHotelService>();
            services.AddTransient<IZoneReservationService, ZoneReservationService>();

            //Add Automapper
            services.AddAutoMapper(typeof(Startup).Assembly);
        }

        

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, AutoMapper.IConfigurationProvider autoMapper)
        {
            autoMapper.AssertConfigurationIsValid();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
                c.DisplayRequestDuration();
                c.EnableFilter();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

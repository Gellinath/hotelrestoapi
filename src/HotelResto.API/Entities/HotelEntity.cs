using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Collections.Generic;

namespace HotelResto.API.Entities
{
    /// <summary>
    /// Hotel entity
    /// </summary>
    [Table("DATA_HOTEL")]
    public class HotelEntity
    {
        /// <summary>
        /// Hotel unique identifier
        /// </summary>
        [Key]
        [Column("HOTEL_UID")]
        public int Id { get; set; }

        /// <summary>
        /// Hotel name
        /// </summary>
        [Column("HOTEL_NAME")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Hotel description
        /// </summary>
        [Column("HOTEL_DESCRIPTION")]
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Hotel num rue
        /// </summary>
        [Column("HOTEL_NUM_RUE")]
        [Required]
        public string NumRue { get; set; }

        /// <summary>
        /// Hotel name rue
        /// </summary>
        [Column("HOTEL_NAME_RUE")]
        [Required]
        public string NameRue { get; set; }

        /// <summary>
        /// Hotel code postal
        /// </summary>
        [Column("HOTEL_CP")]
        [Required]
        public string CP { get; set; }

        /// <summary>
        /// Hotel ville
        /// </summary>
        [Column("HOTEL_VILLE")]
        [Required]
        public string Ville { get; set; }

        /// <summary>
        /// Hotel image
        /// </summary>
        [Column("HOTEL_IMAGE")]
        public string Image { get; set; }

        /// <summary>
        /// Hotel phone number
        /// </summary>
        [Column("HOTEL_NUMBER")]
        public int Number { get; set; }

        /// <summary>
        /// Hotel email
        /// </summary>
        [Column("HOTEL_EMAIL")]
        public string Email { get; set; }

        /// <summary>
        /// Hotel number of rooms available
        /// </summary>
        [Column("HOTEL_ROOMS_AVAILABLE")]
        public int RoomsAvailable { get; set; }

        /// <summary>
        /// Hotel note moyenne
        /// </summary>
        [Column("HOTEL_MOY_NOTE")]
        public int MoyNote { get; set; }
        
        //List of all avis hotel
        public IEnumerable<AvisHotelEntity> AvisHotel { get; set; }

        //List of all chambre
        public IEnumerable<ChambreHotelEntity> ChambreHotel { get; set; }
    }
}
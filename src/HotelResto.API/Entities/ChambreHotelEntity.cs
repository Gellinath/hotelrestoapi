using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelResto.API.Entities
{
    /// <summary>
    /// Chambre hotel entity
    /// </summary>
    [Table("DATA_CHAMBRE_HOTEL")]
    public class ChambreHotelEntity
    {
        /// <summary>
        /// Chambre hotel unique identifier
        /// </summary>
        [Key]
        [Column("CHAMBRE_HOTEL_UID")]
        public int Id { get; set; }

        /// <summary>
        /// Chambre hotel description
        /// </summary>
        [Column("CHAMBRE_HOTEL_DESCRIPTION")]
        [MaxLength(255)]
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Chambre hotel number of people
        /// </summary>
        [Column("CHAMBRE_HOTEL_SIZE")]
        [Required]
        public int Size { get; set; }

        /// <summary>
        /// Chambre hotel foreign key to hotel
        /// </summary>
        [Column("CHAMBRE_HOTEL_HOTEL_ID")]
        [Required]
        public virtual int HotelId { get; set; }
        public HotelEntity Hotel { get; set; }
    }
}
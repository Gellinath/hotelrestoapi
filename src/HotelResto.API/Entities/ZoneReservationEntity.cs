using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelResto.API.Entities
{
    [Table("DATA_ZONERESERVATION")]
    public class ZoneReservationEntity
    {
        [Key]
        [Column("ZONERESERVATION_UID")]
        public int Id {get;set;}

        [Column("HOTEL_UID")]
        [Required]
        public int HotelId {get;set;}

        [Column("UTILISATEUR_UID")]
        [Required]
        public int UtilisateurId {get;set;}

        [Column("ZONERESERVATION_ARRIVE")]
        [Required]
        public DateTime Arrive {get;set;}

        [Column("ZONERESERVATION_DEPART")]
        [Required]
        public DateTime Depart {get;set;}

        [Column("ZONERESERVATION_NBR_CHAMBRE")]
        [Required]
        public int NbrChambre {get;set;}

        [Column("ZONERESERVATION_NBR_PERSONNE")]
        [Required]
        public int NbrPersonne {get;set;}
    }
}
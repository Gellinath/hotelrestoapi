using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelResto.API.Entities
{
    [Table("DATA_RESTAURANT")]
    public class RestaurantEntity
    {
        /// <summary>
        /// Restaurant UID
        /// </summary>
        /// <value></value>
        [Key]
        [Column("RESTAURANT_UID")]
        public int Id {get;set;}

        /// <summary>
        /// Restaurant name
        /// </summary>
        /// <value></value>
        [Column("RESTAURANT_NAME")]
        [Required]
        public string Name {get; set;}

        /// <summary>
        /// Restaurant description
        /// </summary>
        /// <value></value>
        [Column("RESTAURANT_DESCRIPTION")]
        [Required]
        public string Description {get; set;}

        /// <summary>
        /// Restaurant image
        /// </summary>
        /// <value></value>
        [Column("RESTAURANT_IMAGE")]
        public string Image {get; set;}

        /// <summary>
        /// Restaurant téléphone
        /// </summary>
        /// <value></value>
        [Column("RESTAURANT_TELEPHONE")]
        public string Telephone {get; set;}

        /// <summary>
        /// Restaurant Email
        /// </summary>
        /// <value></value>
        [Column("RESTAURANT_EMAIL")]
        public string Email {get; set;}

        /// <summary>
        /// Restaurant num rue
        /// </summary>
        [Column("RESTAURANT_NUM_RUE")]
        [Required]
        public string NumRue { get; set; }

        /// <summary>
        /// Restaurant name rue
        /// </summary>
        [Column("RESTAURANT_NAME_RUE")]
        [Required]
        public string NameRue { get; set; }

        /// <summary>
        /// Restaurant code postal
        /// </summary>
        [Column("RESTAURANT_CP")]
        [Required]
        public string CP { get; set; }

        /// <summary>
        /// Restaurant ville
        /// </summary>
        [Column("RESTAURANT_VILLE")]
        [Required]
        public string Ville { get; set; }

        /// <summary>
        /// Restaurant moyenne notes
        /// </summary>
        /// <value></value>
        [Column("RESTAURANT_MOY_NOTE")]
        public double MoyNote {get; set;}

        public IEnumerable<AvisRestaurantEntity> AvisRestaurant { get; set; }
    }
}
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelResto.API.Entities
{
    /// <summary>
    /// Avis hotel entity
    /// </summary>
    [Table("DATA_AVIS_HOTEL")]
    public class AvisHotelEntity
    {
        /// <summary>
        /// Avis hotel unique identifier
        /// </summary>
        [Key]
        [Column("AVIS_HOTEL_UID")]
        public int Id { get; set; }

        /// <summary>
        /// Avis hotel description
        /// </summary>
        [Column("AVIS_HOTEL_DESCRIPTION")]
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Avis hotel Note
        /// </summary>
        [Column("AVIS_HOTEL_NOTE")]
        [Required]
        public int Note { get; set; }

        /// <summary>
        /// Avis hotel foreign key to hotel id
        /// </summary>
        [Column("AVIS_HOTEL_HOTEL_ID")]
        [Required]
        public int HotelId { get; set; }

        public HotelEntity Hotel { get; set; }
    }
}
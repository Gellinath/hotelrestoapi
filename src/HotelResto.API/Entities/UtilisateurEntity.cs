using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelResto.API.Entities
{
    [Table("DATA_UTILISATEUR")]
    public class UtilisateurEntity
    {
        [Key]
        [Column("UTILISATEUR_UID")]
        public int Id {get;set;}

        [Column("UTILISATEUR_NOM")]
        [Required]
        public string Nom {get;set;}

        [Column("UTILISATEUR_PRENOM")]
        [Required]
        public string Prenom {get;set;}

        [Column("UTILISATEUR_TELEPHONE")]
        [Required]
        public string Telephone {get;set;}

        [Column("UTILISATEUR_EMAIL")]
        [Required]
        public string Email {get;set;}

        [Column("UTILISATEUR_LOGIN")]
        [Required]
        public string Login {get;set;}

        [Column("UTILISATEUR_MDP")]
        [Required]
        public string MDP {get;set;}

        [Column("CONNECTED")]
        public bool Connected { get; set; }
    }
}
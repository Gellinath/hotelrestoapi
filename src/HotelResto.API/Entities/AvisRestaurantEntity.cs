using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HotelResto.API.Entities
{
    [Table("DATA_AVIS_RESTAURANT")]
    public class AvisRestaurantEntity
    {
        /// <summary>
        /// Avis restaurant unique identifier
        /// </summary>
        [Key]
        [Column("AVIS_RESTAURANT_UID")]
        public int Id { get; set; }

        /// <summary>
        /// Avis restaurant description
        /// </summary>
        [Column("AVIS_RESTAURANT_DESCRIPTION")]
        [Required]
        public string Description { get; set; }

        /// <summary>
        /// Avis restaurant Note
        /// </summary>
        [Column("AVIS_RESTAURANT_NOTE")]
        [Required]
        public int Note { get; set; }

        /// <summary>
        /// Avis restaurant foreign key to restaurant id
        /// </summary>
        [Column("AVIS_RESTAURANT_RESTAURANT_ID")]
        [Required]
        public int RestaurantId { get; set; }

        public RestaurantEntity Restaurant { get; set; }
    }
}
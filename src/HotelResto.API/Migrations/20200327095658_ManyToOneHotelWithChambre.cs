﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelResto.API.Migrations
{
    public partial class ManyToOneHotelWithChambre : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "CHAMBRE_HOTEL_HOTEL_ID",
                table: "DATA_CHAMBRE_HOTEL",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_DATA_CHAMBRE_HOTEL_CHAMBRE_HOTEL_HOTEL_ID",
                table: "DATA_CHAMBRE_HOTEL",
                column: "CHAMBRE_HOTEL_HOTEL_ID");

            migrationBuilder.AddForeignKey(
                name: "FK_DATA_CHAMBRE_HOTEL_DATA_HOTEL_CHAMBRE_HOTEL_HOTEL_ID",
                table: "DATA_CHAMBRE_HOTEL",
                column: "CHAMBRE_HOTEL_HOTEL_ID",
                principalTable: "DATA_HOTEL",
                principalColumn: "HOTEL_UID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_DATA_CHAMBRE_HOTEL_DATA_HOTEL_CHAMBRE_HOTEL_HOTEL_ID",
                table: "DATA_CHAMBRE_HOTEL");

            migrationBuilder.DropIndex(
                name: "IX_DATA_CHAMBRE_HOTEL_CHAMBRE_HOTEL_HOTEL_ID",
                table: "DATA_CHAMBRE_HOTEL");

            migrationBuilder.DropColumn(
                name: "CHAMBRE_HOTEL_HOTEL_ID",
                table: "DATA_CHAMBRE_HOTEL");
        }
    }
}

﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelResto.API.Migrations
{
    public partial class Initital3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "DATA_CHAMBRE_HOTEL",
                columns: table => new
                {
                    CHAMBRE_HOTEL_UID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    CHAMBRE_HOTEL_DESCRIPTION = table.Column<string>(maxLength: 255, nullable: false),
                    CHAMBRE_HOTEL_SIZE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DATA_CHAMBRE_HOTEL", x => x.CHAMBRE_HOTEL_UID);
                });

            migrationBuilder.CreateTable(
                name: "DATA_HOTEL",
                columns: table => new
                {
                    HOTEL_UID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    HOTEL_NAME = table.Column<string>(nullable: false),
                    HOTEL_DESCRIPTION = table.Column<string>(nullable: false),
                    HOTEL_NUM_RUE = table.Column<string>(nullable: false),
                    HOTEL_NAME_RUE = table.Column<string>(nullable: false),
                    HOTEL_CP = table.Column<string>(nullable: false),
                    HOTEL_VILLE = table.Column<string>(nullable: false),
                    HOTEL_IMAGE = table.Column<string>(nullable: true),
                    HOTEL_NUMBER = table.Column<int>(nullable: false),
                    HOTEL_EMAIL = table.Column<string>(nullable: true),
                    HOTEL_ROOMS_AVAILABLE = table.Column<int>(nullable: false),
                    HOTEL_MOY_NOTE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DATA_HOTEL", x => x.HOTEL_UID);
                });

            migrationBuilder.CreateTable(
                name: "DATA_RESTAURANT",
                columns: table => new
                {
                    RESTAURANT_UID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    RESTAURANT_NAME = table.Column<string>(nullable: false),
                    RESTAURANT_DESCRIPTION = table.Column<string>(nullable: false),
                    RESTAURANT_IMAGE = table.Column<string>(nullable: true),
                    RESTAURANT_TELEPHONE = table.Column<string>(nullable: true),
                    RESTAURANT_EMAIL = table.Column<string>(nullable: true),
                    RESTAURANT_NUM_RUE = table.Column<string>(nullable: false),
                    RESTAURANT_NAME_RUE = table.Column<string>(nullable: false),
                    RESTAURANT_CP = table.Column<string>(nullable: false),
                    RESTAURANT_VILLE = table.Column<string>(nullable: false),
                    RESTAURANT_MOY_NOTE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DATA_RESTAURANT", x => x.RESTAURANT_UID);
                });

            migrationBuilder.CreateTable(
                name: "DATA_UTILISATEUR",
                columns: table => new
                {
                    UTILISATEUR_UID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    UTILISATEUR_NOM = table.Column<string>(nullable: false),
                    UTILISATEUR_PRENOM = table.Column<string>(nullable: false),
                    UTILISATEUR_TELEPHONE = table.Column<string>(nullable: false),
                    UTILISATEUR_EMAIL = table.Column<string>(nullable: false),
                    UTILISATEUR_LOGIN = table.Column<string>(nullable: false),
                    UTILISATEUR_MDP = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DATA_UTILISATEUR", x => x.UTILISATEUR_UID);
                });

            migrationBuilder.CreateTable(
                name: "DATA_ZONERESERVATION",
                columns: table => new
                {
                    ZONERESERVATION_UID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    HOTEL_UID = table.Column<int>(nullable: false),
                    UTILISATEUR_UID = table.Column<int>(nullable: false),
                    ZONERESERVATION_ARRIVE = table.Column<DateTime>(nullable: false),
                    ZONERESERVATION_DEPART = table.Column<DateTime>(nullable: false),
                    ZONERESERVATION_NBR_CHAMBRE = table.Column<int>(nullable: false),
                    ZONERESERVATION_NBR_PERSONNE = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DATA_ZONERESERVATION", x => x.ZONERESERVATION_UID);
                });

            migrationBuilder.CreateTable(
                name: "DATA_AVIS_HOTEL",
                columns: table => new
                {
                    AVIS_HOTEL_UID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AVIS_HOTEL_DESCRIPTION = table.Column<string>(nullable: false),
                    AVIS_HOTEL_NOTE = table.Column<int>(nullable: false),
                    AVIS_HOTEL_HOTEL_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DATA_AVIS_HOTEL", x => x.AVIS_HOTEL_UID);
                    table.ForeignKey(
                        name: "FK_DATA_AVIS_HOTEL_DATA_HOTEL_AVIS_HOTEL_HOTEL_ID",
                        column: x => x.AVIS_HOTEL_HOTEL_ID,
                        principalTable: "DATA_HOTEL",
                        principalColumn: "HOTEL_UID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "DATA_AVIS_RESTAURANT",
                columns: table => new
                {
                    AVIS_RESTAURANT_UID = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    AVIS_RESTAURANT_DESCRIPTION = table.Column<string>(nullable: false),
                    AVIS_RESTAURANT_NOTE = table.Column<int>(nullable: false),
                    AVIS_RESTAURANT_RESTAURANT_ID = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_DATA_AVIS_RESTAURANT", x => x.AVIS_RESTAURANT_UID);
                    table.ForeignKey(
                        name: "FK_DATA_AVIS_RESTAURANT_DATA_RESTAURANT_AVIS_RESTAURANT_RESTAUR~",
                        column: x => x.AVIS_RESTAURANT_RESTAURANT_ID,
                        principalTable: "DATA_RESTAURANT",
                        principalColumn: "RESTAURANT_UID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_DATA_AVIS_HOTEL_AVIS_HOTEL_HOTEL_ID",
                table: "DATA_AVIS_HOTEL",
                column: "AVIS_HOTEL_HOTEL_ID");

            migrationBuilder.CreateIndex(
                name: "IX_DATA_AVIS_RESTAURANT_AVIS_RESTAURANT_RESTAURANT_ID",
                table: "DATA_AVIS_RESTAURANT",
                column: "AVIS_RESTAURANT_RESTAURANT_ID");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "DATA_AVIS_HOTEL");

            migrationBuilder.DropTable(
                name: "DATA_AVIS_RESTAURANT");

            migrationBuilder.DropTable(
                name: "DATA_CHAMBRE_HOTEL");

            migrationBuilder.DropTable(
                name: "DATA_UTILISATEUR");

            migrationBuilder.DropTable(
                name: "DATA_ZONERESERVATION");

            migrationBuilder.DropTable(
                name: "DATA_HOTEL");

            migrationBuilder.DropTable(
                name: "DATA_RESTAURANT");
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelResto.API.Migrations
{
    public partial class update001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<double>(
                name: "RESTAURANT_MOY_NOTE",
                table: "DATA_RESTAURANT",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "RESTAURANT_MOY_NOTE",
                table: "DATA_RESTAURANT",
                type: "int",
                nullable: false,
                oldClrType: typeof(double));
        }
    }
}

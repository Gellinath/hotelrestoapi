﻿// <auto-generated />
using System;
using HotelResto.API.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace HotelResto.API.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    [Migration("20200327170015_update002")]
    partial class update002
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.3")
                .HasAnnotation("Relational:MaxIdentifierLength", 64);

            modelBuilder.Entity("HotelResto.API.Entities.AvisHotelEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("AVIS_HOTEL_UID")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnName("AVIS_HOTEL_DESCRIPTION")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<int>("HotelId")
                        .HasColumnName("AVIS_HOTEL_HOTEL_ID")
                        .HasColumnType("int");

                    b.Property<int>("Note")
                        .HasColumnName("AVIS_HOTEL_NOTE")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("HotelId");

                    b.ToTable("DATA_AVIS_HOTEL");
                });

            modelBuilder.Entity("HotelResto.API.Entities.AvisRestaurantEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("AVIS_RESTAURANT_UID")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnName("AVIS_RESTAURANT_DESCRIPTION")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<int>("Note")
                        .HasColumnName("AVIS_RESTAURANT_NOTE")
                        .HasColumnType("int");

                    b.Property<int>("RestaurantId")
                        .HasColumnName("AVIS_RESTAURANT_RESTAURANT_ID")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("RestaurantId");

                    b.ToTable("DATA_AVIS_RESTAURANT");
                });

            modelBuilder.Entity("HotelResto.API.Entities.ChambreHotelEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("CHAMBRE_HOTEL_UID")
                        .HasColumnType("int");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnName("CHAMBRE_HOTEL_DESCRIPTION")
                        .HasColumnType("varchar(255) CHARACTER SET utf8mb4")
                        .HasMaxLength(255);

                    b.Property<int>("HotelId")
                        .HasColumnName("CHAMBRE_HOTEL_HOTEL_ID")
                        .HasColumnType("int");

                    b.Property<int>("Size")
                        .HasColumnName("CHAMBRE_HOTEL_SIZE")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("HotelId");

                    b.ToTable("DATA_CHAMBRE_HOTEL");
                });

            modelBuilder.Entity("HotelResto.API.Entities.HotelEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("HOTEL_UID")
                        .HasColumnType("int");

                    b.Property<string>("CP")
                        .IsRequired()
                        .HasColumnName("HOTEL_CP")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnName("HOTEL_DESCRIPTION")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Email")
                        .HasColumnName("HOTEL_EMAIL")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Image")
                        .HasColumnName("HOTEL_IMAGE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<int>("MoyNote")
                        .HasColumnName("HOTEL_MOY_NOTE")
                        .HasColumnType("int");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("HOTEL_NAME")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("NameRue")
                        .IsRequired()
                        .HasColumnName("HOTEL_NAME_RUE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("NumRue")
                        .IsRequired()
                        .HasColumnName("HOTEL_NUM_RUE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<int>("Number")
                        .HasColumnName("HOTEL_NUMBER")
                        .HasColumnType("int");

                    b.Property<int>("RoomsAvailable")
                        .HasColumnName("HOTEL_ROOMS_AVAILABLE")
                        .HasColumnType("int");

                    b.Property<string>("Ville")
                        .IsRequired()
                        .HasColumnName("HOTEL_VILLE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.ToTable("DATA_HOTEL");
                });

            modelBuilder.Entity("HotelResto.API.Entities.RestaurantEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("RESTAURANT_UID")
                        .HasColumnType("int");

                    b.Property<string>("CP")
                        .IsRequired()
                        .HasColumnName("RESTAURANT_CP")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Description")
                        .IsRequired()
                        .HasColumnName("RESTAURANT_DESCRIPTION")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Email")
                        .HasColumnName("RESTAURANT_EMAIL")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Image")
                        .HasColumnName("RESTAURANT_IMAGE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<double>("MoyNote")
                        .HasColumnName("RESTAURANT_MOY_NOTE")
                        .HasColumnType("double");

                    b.Property<string>("Name")
                        .IsRequired()
                        .HasColumnName("RESTAURANT_NAME")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("NameRue")
                        .IsRequired()
                        .HasColumnName("RESTAURANT_NAME_RUE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("NumRue")
                        .IsRequired()
                        .HasColumnName("RESTAURANT_NUM_RUE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Telephone")
                        .HasColumnName("RESTAURANT_TELEPHONE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Ville")
                        .IsRequired()
                        .HasColumnName("RESTAURANT_VILLE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.ToTable("DATA_RESTAURANT");
                });

            modelBuilder.Entity("HotelResto.API.Entities.UtilisateurEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("UTILISATEUR_UID")
                        .HasColumnType("int");

                    b.Property<string>("Email")
                        .IsRequired()
                        .HasColumnName("UTILISATEUR_EMAIL")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Login")
                        .IsRequired()
                        .HasColumnName("UTILISATEUR_LOGIN")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("MDP")
                        .IsRequired()
                        .HasColumnName("UTILISATEUR_MDP")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Nom")
                        .IsRequired()
                        .HasColumnName("UTILISATEUR_NOM")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Prenom")
                        .IsRequired()
                        .HasColumnName("UTILISATEUR_PRENOM")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.Property<string>("Telephone")
                        .IsRequired()
                        .HasColumnName("UTILISATEUR_TELEPHONE")
                        .HasColumnType("longtext CHARACTER SET utf8mb4");

                    b.HasKey("Id");

                    b.ToTable("DATA_UTILISATEUR");
                });

            modelBuilder.Entity("HotelResto.API.Entities.ZoneReservationEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnName("ZONERESERVATION_UID")
                        .HasColumnType("int");

                    b.Property<DateTime>("Arrive")
                        .HasColumnName("ZONERESERVATION_ARRIVE")
                        .HasColumnType("datetime(6)");

                    b.Property<DateTime>("Depart")
                        .HasColumnName("ZONERESERVATION_DEPART")
                        .HasColumnType("datetime(6)");

                    b.Property<int>("HotelId")
                        .HasColumnName("HOTEL_UID")
                        .HasColumnType("int");

                    b.Property<int>("NbrChambre")
                        .HasColumnName("ZONERESERVATION_NBR_CHAMBRE")
                        .HasColumnType("int");

                    b.Property<int>("NbrPersonne")
                        .HasColumnName("ZONERESERVATION_NBR_PERSONNE")
                        .HasColumnType("int");

                    b.Property<int>("UtilisateurId")
                        .HasColumnName("UTILISATEUR_UID")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.ToTable("DATA_ZONERESERVATION");
                });

            modelBuilder.Entity("HotelResto.API.Entities.AvisHotelEntity", b =>
                {
                    b.HasOne("HotelResto.API.Entities.HotelEntity", "Hotel")
                        .WithMany("AvisHotel")
                        .HasForeignKey("HotelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("HotelResto.API.Entities.AvisRestaurantEntity", b =>
                {
                    b.HasOne("HotelResto.API.Entities.RestaurantEntity", "Restaurant")
                        .WithMany("AvisRestaurant")
                        .HasForeignKey("RestaurantId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("HotelResto.API.Entities.ChambreHotelEntity", b =>
                {
                    b.HasOne("HotelResto.API.Entities.HotelEntity", "Hotel")
                        .WithMany("ChambreHotel")
                        .HasForeignKey("HotelId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}

﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace HotelResto.API.Migrations
{
    public partial class UserConnected : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "CONNECTED",
                table: "DATA_UTILISATEUR",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CONNECTED",
                table: "DATA_UTILISATEUR");
        }
    }
}

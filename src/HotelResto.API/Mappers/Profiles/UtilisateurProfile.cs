using AutoMapper;
using HotelResto.API.Dto.Utilisateur;
using HotelResto.API.Entities;

namespace HotelResto.API.Mappers.Profiles
{
    public class UtilisateurProfile : Profile
    {
        public UtilisateurProfile()
        {
            CreateMap<UtilisateurEntity, UtilisateurDTO>();

            CreateMap<UtilisateurCreateDTO, UtilisateurEntity>()
            .ForMember(dest => dest.Nom, opt => opt.MapFrom(src => src.Nom))
            .ForMember(dest => dest.Prenom, opt => opt.MapFrom(src => src.Prenom))
            .ForMember(dest => dest.Telephone, opt => opt.MapFrom(src => src.Telephone))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.Login, opt => opt.MapFrom(src => src.Login))
            .ForMember(dest => dest.MDP, opt => opt.MapFrom(src => src.MDP))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<UtilisateurLoginDTO, UtilisateurEntity>()
            .ForMember(dest => dest.Login, opt => opt.MapFrom(src => src.Login))
            .ForMember(dest => dest.MDP, opt => opt.MapFrom(src => src.MDP))
            .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
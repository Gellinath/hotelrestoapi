using AutoMapper;

namespace HotelResto.API.Mappers.Profiles
{
    public class AutoMapperConfiguration
    {
        public static MapperConfiguration RegisterMappings()
        {
            return new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new RestaurantProfile());
                cfg.AddProfile(new AvisRestaurantProfile());
                cfg.AddProfile(new HotelProfile());
                cfg.AddProfile(new AvisHotelProfile());
                cfg.AddProfile(new UtilisateurProfile());
                cfg.AddProfile(new ChambreHotelProfile());
                cfg.AddProfile(new ZoneReservationProfile());
            });
        }
    }
}
using AutoMapper;
using HotelResto.API.Dto.ZoneReservation;
using HotelResto.API.Entities;

namespace HotelResto.API.Mappers.Profiles
{
    public class ZoneReservationProfile : Profile
    {
        public ZoneReservationProfile()
        {
            CreateMap<ZoneReservationEntity, ZoneReservationDTO>();

            CreateMap<ZoneReservationCreateDTO, ZoneReservationEntity>()
            .ForMember(dest => dest.HotelId, opt => opt.MapFrom(src => src.HotelId))
            .ForMember(dest => dest.Arrive, opt => opt.MapFrom(src => src.Arrive))
            .ForMember(dest => dest.Depart, opt => opt.MapFrom(src => src.Depart))
            .ForMember(dest => dest.NbrPersonne, opt => opt.MapFrom(src => src.NbrPersonne))
            .ForMember(dest => dest.NbrChambre, opt => opt.MapFrom(src => src.NbrChambre))
            .ForAllOtherMembers(opt => opt.Ignore());

        }
    }
}
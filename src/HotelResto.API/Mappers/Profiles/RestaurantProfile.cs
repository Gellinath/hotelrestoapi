using AutoMapper;
using HotelResto.API.Dto.Restaurant;
using HotelResto.API.Entities;

namespace HotelResto.API.Mappers.Profiles
{
    public class RestaurantProfile : Profile
    {
        public RestaurantProfile()
        {
            CreateMap<RestaurantEntity, RestaurantDTO>();

            CreateMap<RestaurantDTO, RestaurantEntity>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.Image, opt => opt.MapFrom(src => src.Image))
            .ForMember(dest => dest.Telephone, opt => opt.MapFrom(src => src.Telephone))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.NumRue, opt => opt.MapFrom(src => src.NumRue))
            .ForMember(dest => dest.NameRue, opt => opt.MapFrom(src => src.NameRue))
            .ForMember(dest => dest.CP, opt => opt.MapFrom(src => src.CP))
            .ForMember(dest => dest.Ville, opt => opt.MapFrom(src => src.Ville))
            .ForMember(dest => dest.MoyNote, opt => opt.MapFrom(src => src.MoyNote))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<RestaurantCreateDTO, RestaurantEntity>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.Image, opt => opt.MapFrom(src => src.Image))
            .ForMember(dest => dest.Telephone, opt => opt.MapFrom(src => src.Telephone))
            .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(dest => dest.NumRue, opt => opt.MapFrom(src => src.NumRue))
            .ForMember(dest => dest.NameRue, opt => opt.MapFrom(src => src.NameRue))
            .ForMember(dest => dest.CP, opt => opt.MapFrom(src => src.CP))
            .ForMember(dest => dest.Ville, opt => opt.MapFrom(src => src.Ville))
            .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
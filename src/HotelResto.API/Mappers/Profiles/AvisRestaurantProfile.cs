using AutoMapper;
using HotelResto.API.Dto.Restaurant.AvisRestaurant;
using HotelResto.API.Entities;

namespace HotelResto.API.Mappers.Profiles
{
    public class AvisRestaurantProfile : Profile
    {
        public AvisRestaurantProfile()
        {
            CreateMap<AvisRestaurantEntity, AvisRestaurantDTO>();

            CreateMap<AvisRestaurantCreateDTO, AvisRestaurantEntity>()
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Note))
            .ForMember(dest => dest.RestaurantId, opt => opt.MapFrom(src => src.RestaurantId))
            .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
using AutoMapper;
using HotelResto.API.Dto.Hotel.ChambreHotel;
using HotelResto.API.Entities;

namespace HotelResto.API.Mappers.Profiles
{
    public class ChambreHotelProfile : Profile
    {
        public ChambreHotelProfile()
        {
            //Entity to DTO chambre hotel
            CreateMap<ChambreHotelEntity, ChambreHotelDTO>();

            CreateMap<ChambreHotelCreateDTO, ChambreHotelEntity>()
            .ForMember(src => src.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(src => src.Size, opt => opt.MapFrom(src => src.Size))
            .ForMember(src => src.HotelId, opt => opt.MapFrom(src => src.HotelId))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<ChambreHotelDTO, ChambreHotelEntity>()
            .ForMember(src => src.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(src => src.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(src => src.Size, opt => opt.MapFrom(src => src.Size))
            .ForMember(src => src.HotelId, opt => opt.MapFrom(src => src.HotelId))
            .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
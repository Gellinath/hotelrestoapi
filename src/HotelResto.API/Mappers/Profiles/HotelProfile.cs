using AutoMapper;
using HotelResto.API.Dto.Hotel;
using HotelResto.API.Entities;

namespace HotelResto.API.Mappers.Profiles
{
    /// <summary>
    /// Hotel mapper profile
    /// </summary>
    public class HotelProfile : Profile
    {
        public HotelProfile()
        {
            //Hotel entity to hotel DTO
            CreateMap<HotelEntity, HotelDTO>();

            //Hotel create DTO to entity
            CreateMap<HotelCreateDTO, HotelEntity>()
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.NumRue, opt => opt.MapFrom(src => src.NumRue))
            .ForMember(dest => dest.NameRue, opt => opt.MapFrom(src => src.NameRue))
            .ForMember(dest => dest.CP, opt => opt.MapFrom(src => src.CP))
            .ForMember(dest => dest.Ville, opt => opt.MapFrom(src => src.Ville))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<HotelDTO, HotelEntity>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.NumRue, opt => opt.MapFrom(src => src.NumRue))
            .ForMember(dest => dest.NameRue, opt => opt.MapFrom(src => src.NameRue))
            .ForMember(dest => dest.CP, opt => opt.MapFrom(src => src.CP))
            .ForMember(dest => dest.Ville, opt => opt.MapFrom(src => src.Ville))
            .ForMember(dest => dest.MoyNote, opt => opt.MapFrom(src => src.MoyNote))
            .ForMember(dest => dest.RoomsAvailable, opt => opt.MapFrom(src => src.RoomsAvailable))
            .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
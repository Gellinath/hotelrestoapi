using AutoMapper;
using HotelResto.API.Dto.Hotel.AvisHotel;
using HotelResto.API.Entities;

namespace HotelResto.API.Mappers.Profiles
{
    /// <summary>
    /// Avis hotel mapper
    /// </summary>
    public class AvisHotelProfile : Profile
    {
        public AvisHotelProfile()
        {
            //Entity to DTO
            CreateMap<AvisHotelEntity, AvisHotelDTO>();

            //Avis hotel create DTO to entity
            CreateMap<AvisHotelCreateDTO, AvisHotelEntity>()
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Note))
            .ForMember(dest => dest.HotelId, opt => opt.MapFrom(src => src.HotelId))
            .ForAllOtherMembers(opt => opt.Ignore());

            CreateMap<AvisHotelDTO, AvisHotelEntity>()
            .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
            .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
            .ForMember(dest => dest.Note, opt => opt.MapFrom(src => src.Note))
            .ForMember(dest => dest.HotelId, opt => opt.MapFrom(src => src.HotelId))
            .ForAllOtherMembers(opt => opt.Ignore());
        }
    }
}
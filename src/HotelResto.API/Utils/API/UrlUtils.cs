namespace HotelResto.API.Utils.API
{
    public class UrlUtils
    {
        private const string CURRENT_VERSION = "/v1";

        private const string BASE_API_URL = "api" + CURRENT_VERSION;

        /// <summary>
        /// Nom des ressources
        /// </summary>
        public static class RESSOURCES_NAMES
        {
            public const string RESTAURANT = "restaurants";
            public const string AVIS_RESTAURANT = "avis_restaurants";
            public const string HOTEL = "hotel";
            public const string UTILISATEUR = "utilisateur";
            public const string AVIS_HOTEL = "avis_hotel";
            public const string CHAMBRE = "chambre";
            public const string ZONE_RESERVATION = "zone_reservation";
        }

        /// <summary>
        /// Restaurants ressources
        /// </summary>
        public static class RESTAURANT_RESSOURCE
        {
            /// <summary>
            /// Restaurants
            /// </summary>
            public const string RESTAURANT = BASE_API_URL + "/restaurant";

            /// <summary>
            /// Unique restaurant
            /// </summary>
            /// <value></value>
            public const string RESTAURANT_UNIQUE = RESTAURANT + "/{restaurantId}";

            /// <summary>
            /// Restaurants by ville
            /// </summary>
            /// <value></value>
            public const string RESTAURANT_BY_VILLE = RESTAURANT + "/ville" + "/{ville}";

            /// <summary>
            /// Restaurants by name
            /// </summary>
            /// <value></value>
            public const string RESTAURANT_BY_NAME = RESTAURANT + "/name" + "/{name}";

            /// <summary>
            /// Top 5 restaurant
            /// </summary>
            /// <value></value>
            public const string RESTAURANT_TOP5 = RESTAURANT + "/top5";

            /// <summary>
            /// Avis du restaurant unique
            /// </summary>
            public const string AVIS_RESTAURANT = RESTAURANT_UNIQUE + "/avisRestaurant";

            /// <summary>
            /// Avis unique du restaurant unique
            /// </summary>
            /// <value></value>
            public const string AVIS_RESTAURANT_UNIQUE = AVIS_RESTAURANT + "/{avisRestaurantId}";
        }

        /// <summary>
        /// Hotel ressource
        /// </summary>
        public static class HOTEL_RESSOURCE
        {
            /// <summary>
            /// Hotel
            /// </summary>
            public const string HOTEL = BASE_API_URL + "/hotel";
            
            /// <summary>
            /// Unique hotel
            /// </summary>
            public const string HOTEL_UNIQUE = HOTEL + "/{hotelId}";

            /// <summary>
            /// Hotel by ville
            /// </summary>
            public const string HOTEL_BY_VILLE = HOTEL + "/ville" + "/{ville}";

            /// <summary>
            /// Restaurants by name
            /// </summary>
            public const string HOTEL_BY_NAME = HOTEL + "/name" + "/{name}";

            /// <summary>
            /// Top 5 hotel
            /// </summary>
            public const string HOTEL_TOP5 = HOTEL + "/top5";

            /// <summary>
            /// Avis Hotel
            /// </summary>
            public const string AVIS_HOTEL = HOTEL_UNIQUE + "/avisHotel";

            /// <summary>
            /// Avis hotel unique
            /// </summary>
            public const string AVIS_HOTEL_UNIQUE = AVIS_HOTEL + "/{avisHotelId}";

            /// <summary>
            /// Chambre hotel
            /// </summary>
            public const string CHAMBRE = HOTEL_UNIQUE + "/chambre";

            /// <summary>
            /// Chambre hotel unique
            /// </summary>
            public const string CHAMBRE_UNIQUE = CHAMBRE + "/{chambreId}";
        }

        /// <summary>
        /// Utilisateurs ressources
        /// </summary>
        public static class UTILISATEUR_RESSOURCE
        {
            /// <summary>
            /// Utilisateur
            /// </summary>
            public const string UTILISATEUR = BASE_API_URL + "/utilisateur";

            /// <summary>
            /// Utilisateur connection
            /// </summary>
            public const string UTILISATEUR_CONNECTION = UTILISATEUR + "/connection";

            /// <summary>
            /// Unique utilisateur
            /// </summary>
            /// <value></value>
            public const string UTILISATEUR_UNIQUE = UTILISATEUR + "/{utilisateurId}";

            /// <summary>
            /// Unique utilisateur disconnect
            /// </summary>
            /// <value></value>
            public const string UTILISATEUR_UNIQUE_DISCONNECT = UTILISATEUR_UNIQUE + "/disconnect";

        }

        /// <summary>
        /// Zone reservation ressources
        /// </summary>
        public static class ZONE_RESERVATION_RESSOURCE
        {
            /// <summary>
            /// Zone reservation user
            /// </summary>
            public const string ZONE_RESERVATION_USER = BASE_API_URL + "/zone_reservation" + "/{utilisateurId}";

            /// <summary>
            /// All zone reservation possible 
            /// </summary>
            public const string ALL_RESERVATION = ZONE_RESERVATION_USER + "/possible";
            
            /// <summary>
            /// Zone reservation possible by hotel id
            /// </summary>
            public const string ALL_RESERVATION_HOTEL_ID = ZONE_RESERVATION_USER + "/possibleByHotelId";

            /// <summary>
            /// Zone reservation possible by hotel name
            /// </summary>
            public const string ALL_RESERVATION_HOTEL_NAME = ZONE_RESERVATION_USER + "/possibleByHotelName";

            /// <summary>
            /// Zone reservation possible by hotel id
            /// </summary>
            public const string ALL_RESERVATION_HOTEL_VILLE = ZONE_RESERVATION_USER + "/possibleByHotelVille";

            /// <summary>
            /// Unique zone reservation user
            /// </summary>
            /// <value></value>
            public const string ZONE_RESERVATION_USER_UNIQUE = ZONE_RESERVATION_USER + "/{zone_reservationId}";

        }
    }
}
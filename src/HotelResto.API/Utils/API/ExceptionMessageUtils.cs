namespace HotelResto.API.Utils.API
{
    public class ExceptionMessageUtils
    {
        /// <summary>
        /// Forbidden exception message
        /// </summary>
        public const string FORBIDDEN = "you don't have access rights.";

        /// <summary>
        /// Not found exception message
        /// </summary>
        public const string NOT_FOUND = "No result.";

        /// <summary>
        /// Get failed exception message
        /// </summary>
        public const string GET_FAILED = "Error getting.";

        /// <summary>
        /// Add failed exception message
        /// </summary>
        public const string ADD_FAILED = "Error adding.";

        /// <summary>
        /// Update failed exception message
        /// </summary>
        public const string UPDATE_FAILED = "Error updating.";

        /// <summary>
        /// Delete failed exception message
        /// </summary>
        public const string DELETE_FAILED = "Error deleting.";

    }
}